import pandas as pd
from bs4 import BeautifulSoup
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import statistics
import numpy as np
html_string = '''



<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html lang="pl-PL">
   <head><title>LIBRUS Synergia</title>
   <meta http-equiv="Content-type" content="text/html; charset=UTF-8">
   <meta http-equiv="Reply-to" content="administrator@dziennik.librus.pl">
   <meta http-equiv="Content-Language" content="pl">
   <meta name="Keywords" content="dziennik, elektroniczny, edukacja, oświata, programy, komputerowe, edziennik, e-dziennik, synergia">
   <meta name="Description" content="Dziennik elektroniczny">
   <meta name="robots" content="noindex, nofollow" />
        <link rel="shortcut icon" href="/images/synergia.ico">
           <!-- Google Analytics -->
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-3618915-24', {'sampleRate': 2.5});
    ga('send', 'pageview');
    </script>
    <!-- End Google Analytics -->            <link rel="stylesheet" href="/js/jquery/jquery-ui-1.9.2.custom/css/librus/jquery-ui-1.9.2.custom.min.css" />
        <link rel="stylesheet" href="/js/jquery/jquery-ui-1.9.2.custom/css/librus/librus-aditional.css" />
                <script type="text/javascript" src="/js/jquery/jquery-ui-1.9.2.custom/js/jquery-1.8.3.js" ></script>
        <script type="text/javascript" src="/js/jquery/jquery-ui-1.9.2.custom/js/jquery-ui-1.9.2.custom.min.js"></script>
        <script type="text/javascript" src="/js/newLayout/jquery.mask.min.js"></script>
        <script type="text/javascript" src="/js/LibrusJQueryTheme.js"></script>
        <script type="text/javascript" src="/js/rowCollection.js"></script>
        <script type="text/javascript" src="/js/quickSearchTool.js"></script>
        <script type="text/javascript" src="/js/CsrfProtection.js"></script>
        <script type="text/javascript">
            var csrfTokenName = "requestkey";
            var csrfTokenValue = "MC41MDE0MzgwMCAxNTU2NDU4NDY4XzdiYzcyNmE1YTdlZDQ3NmE0YTM4Y2EwYjk0N2Y5MmJk";
        </script>
            <script type='text/javascript'>
        var APP_SETTINGS = new Object();
        Object.defineProperties(APP_SETTINGS, {
            idw_content_domain: {
                value: "https://synergia.indywidualni.pl",
                enumerable: true
            }
        });
    </script>
    <script type="text/javascript" src="/js/fileValidator.1535145151.js"></script><script type="text/javascript" src="/js/CloudStorageBrowseTool.1507325373.js"></script><script type="text/javascript" src="/js/indywidualni.1507325373.js"></script><script type="text/javascript" src="/js/jquery/fancybox/jquery.fancybox-1.3.4.1332493810.js"></script><script type="text/javascript" src="/js/connectRMNToLesson.1507325373.js"></script>        <script type="text/javascript">
            CSRF_PROTECTION.startAddingCsrfTokenTo($, csrfTokenName, csrfTokenValue);
        </script>
           <script src="/js/jquery/jquery.form.js"></script>
   <script src="/js/jquery/jquery-ui-timepicker-addon.js"></script><script src="/js/datatables.js"></script>   <script src="/assets/js/fancybox/jquery.fancybox-1.3.4.pack.js" type="text/javascript"></script>
   <link href="/assets/css/fancybox/jquery.fancybox-1.3.4.css" rel="stylesheet" type="text/css"/>
   <script>// HOTFIX: We can't upgrade to jQuery UI 1.8.6 (yet)
    // This hotfix makes older versions of jQuery UI drag-and-drop work in IE9
    (function($){var a=$.ui.mouse.prototype._mouseMove;$.ui.mouse.prototype._mouseMove=function(b){if($.browser.msie&&document.documentMode>=9){b.button=1};a.apply(this,[b]);}}(jQuery));</script>
      


           <link rel="stylesheet" href="/LibrusStyleSheet2.1555536903.css" type="text/css" media="screen">        <link rel="stylesheet" href="/LibrusPrintStyleSheet2.1495831305.css" type="text/css" media="print">        <!--[if !IE]><!-->
        <link rel="stylesheet" href="/LibrusStyleSheet2NonIE.1361960241.css" type="text/css" media="screen">        <!--<![endif]-->
        <!--[if IE]>
        <link rel="stylesheet" href="/LibrusStyleSheet2IE.1368167439.css" type="text/css" media="screen">        <![endif]-->
        <!--[if IE 7]>
        <link rel="stylesheet" href="/LibrusStyleSheet2IE7.1371460280.css" type="text/css" media="screen">        <![endif]-->
        <!--[if IE 8]>
        <link rel="stylesheet" href="/LibrusStyleSheet2IE8.1367217179.css" type="text/css" media="screen">        <![endif]-->
        <!--[if IE 9]>
        <link rel="stylesheet" href="/LibrusStyleSheet2IE9.1366628972.css" type="text/css" media="screen">        <![endif]-->
        <link rel="stylesheet" href="/assets/css/synergia.1535145152.css" type="text/css" media="screen"><link rel="stylesheet" href="/assets/css/style.1551481515.css" type="text/css" media="screen"><script type="text/javascript" src="/assets/js/synergia/main.1438948866.js"></script>        <script type="text/javascript">
            var appName1 = 'LIBRUS Synergia';
            var appName2 = 'LIBRUS Synergia';
            var appName3 = 'LIBRUS Synergia';
            var appName4 = 'LIBRUS Synergia';
            var appName5 = 'LIBRUS Synergia';
            var appName6 = 'LIBRUS Synergia';
            var appName7 = 'LIBRUS Synergia';
        </script>
        <link rel="stylesheet" type="text/css" media="screen" href="/js/newLayout/SparkBoxSelect/sparkbox-select.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="/jquery.treeview.css" />

            <script type="text/javascript" src="/js/cookie.js" ></script>
       <script type="text/javascript" src="/js/uklad_strony2.1544825514.js"></script>       <script type="text/javascript" src="/js/ZeroClipboard.min.js"></script>
       <script type="text/javascript">
           ZeroClipboard.setDefaults( { moviePath: '/swf/ZeroClipboard.swf' } );
       </script>


   <script type="text/javascript" src="/js/newLayout/SparkBoxSelect/jquery.sparkbox-select.js" ></script>
        <link rel="stylesheet" href="/js/jquery/mTip.black.css" />
        <script src="/js/newLayout/amCharts.js" type="text/javascript"></script>
        <script src="/js/validators.js" type="text/javascript" ></script>
        <script src="/js/ErrorProvider.js" type="text/javascript"></script>
        <script src="/js/jquery/mTip-v1.0.3.js" type="text/javascript"></script>
        <script src="/js/newLayout/rgbcolor.js" type="text/javascript"></script>
    <script type="text/javascript" src="/modules/Sekretariat/js/SKFOldInterface/Utils.1551481514.js"></script><script type="text/javascript" src="/js/newLayout/newLayoutJS.1526677911.js"></script><script type="text/javascript" src="/js/newLayout/newLayoutJSExecutes.1507325373.js"></script>    <!--[if IE 7]>
    <script type="text/javascript" src="/js/newLayout/newLayoutJSIE7.1368708313.js"></script>    <![endif]-->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/js/newLayout/newLauoutJSltIE9.1375344710.js"></script>    <![endif]-->
    <link rel="stylesheet" href="/js/jquery/jquery.multiselect.1389284078.css" type="text/css">    <script type="text/javascript" src="/js/jquery/jquery.multiselect.1422664051.js"></script>    <script type="text/javascript" src="/assets/js/synergia/MultiselectLibrus.1495831304.js"></script>    <script type="text/javascript" src="/js/pobierz_date_wywiadowki2.1495831305.js"></script>    <script type="text/javascript" src="/js/pobierz_email4.1535749794.js"></script>       <script type="text/javascript" src="/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
   <link rel="stylesheet" type="text/css" href="/js/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
   <!--[if lt IE 9]>
   <script type="text/javascript">
   $(document).ready(function(){
        $(window).on("resize", function(){
            simMediaQuery();
        });

        simMediaQuery();

        function simMediaQuery(){
            var widthTrigger = 1100;
            var viewportW = parseInt($(window).width());
            if (viewportW > widthTrigger){
                if( $('#interfejs-lekcyjny').length <= 0 || parseInt( $('#body').css("padding-left") ) < 50 ) {
                    $('#body').css({"padding-left" : "50px"});
                }
                $('#body').css({"padding-right" : "50px"});
            } else {
                if( $('#interfejs-lekcyjny').length <= 0 ) {
                    $('#body').css({"padding-left" : "0"});
                }
                $('#body').css({"padding-right" : "0"});
            }
        }

    });
   </script>
   <![endif]-->
   <script type="text/javascript" src="/ajax.js"></script>
   <script type="text/javascript">

	<!--
	var ajax = new sack();
	var ajax1 = new sack();
	var ajax2 = new sack();
	var ajax3 = new sack();
	var ajax4 = new sack();
	var ajaxLesBar = new sack();
	var ajaxKomMenu = new sack();
	var edit_success = false; // wykorzystywane w onload przy hospitacjach
	var isInterfaceMinimalize = 0;
	var isInterfaceHide = 0;

	/*
       Function: getLessonsList

       Function gets lessons list.

       Author:
            Dariusz Kozka <dariusz.kozka[at]librus.pl>

       Parameters:

            int 	sel - idetifier of class.
            int 	pick - identifier of selected lesson.

    */
	function getLessonsList(sel, pick)
	{
		var klasa = sel;
		if(klasa > 0)
		{
			if(pick)
			{
				var selected = '&selected=' + pick;
			}
			else
			{
				var selected = '';
			}
			document.getElementById('lekcja').options.length = 0;
			if(klasa.length>0)
			{
				ajax2.requestFile = "https:\/\/synergia.librus.pl" + '/ajax/combos_lekcje.php?klasa=' + klasa + selected;
				ajax2.onCompletion = createLessons;
				ajax2.onLoading = showLessonWaitMessage;
				ajax2.runAJAX();
			}
		}
	}

    /*
       Function: createLessons

       Function create lessons.

       Author:
            Dariusz Kozka <dariusz.kozka[at]librus.pl>

    */
	function createLessons()
	{
		document.getElementById('lekcja').options.length = 0;
		var obj = document.getElementById('lekcja');
		eval(ajax2.response);
	}

    /*
       Function: showLessonWaitMessage

       Function shows lesson wait message.

       Author:
            Dariusz Kozka <dariusz.kozka[at]librus.pl>

    */
	function showLessonWaitMessage()
	{
		document.getElementById('lekcja').options[document.getElementById('lekcja').options.length] = new Option('Trwa generowanie listy...', '');
	}

    /*
       Function: hideSubmenuLayer

       Function hide submenu layer.

       Author:
            Dariusz Kozka <dariusz.kozka[at]librus.pl>

    */
	function hideSubmenuLayer()
	{
		var tool = document.getElementById('tooltip');
		if(tool)
		{
			document.body.removeChild(tool);
		}
		var ajax_loader = '<div style="text-align: center;">Ładowanie ustawień ...</div>';
		ajax.requestFile = "https:\/\/synergia.librus.pl" + '/ajax/pozycje_obiektow.php?action=hide&objectType=interfejsSubmenu&isInterfaceMin='+isInterfaceMinimalize;
		ajax.onCompletion = loadCoords;
		ajax.onLoading = showWaitMessage;
		ajax.runAJAX();
		dd.elements.interfejsSubmenu.hide(true);
		function showWaitMessage()
		{
			document.getElementById('interfejsSubmenu').innerHTML = ajax_loader;
		}

		function loadCoords()
		{
			document.getElementById('interfejsSubmenu').innerHTML = ajax.response;
		}
	}

    /*
       Function: minimalizeInterface

       Function minimalize interface.

       Author:
            Dariusz Kozka <dariusz.kozka[at]librus.pl>

    */
	function minimalizeInterface()
	{
		var menu = document.getElementById('interfejsmenu');
		var minImg = document.getElementById('minimalizeInterfaceIMG');
		var maxImg = document.getElementById('maximalizeInterfaceIMG');
		if(menu && maxImg && minImg){
			menu.style.display = 'none';
			minImg.style.display = 'none';
			maxImg.style.display = '';
			isInterfaceMinimalize = 1;
			ajax4.requestFile = "https:\/\/synergia.librus.pl" + '/ajax/stan_minimalizacji_interfejsu.php?isInterfaceMin='+isInterfaceMinimalize;
			ajax4.runAJAX();

		}
	}

    /*
       Function: maximalizeInterface

       Function maximalize interface.

       Author:
            Dariusz Kozka <dariusz.kozka[at]librus.pl>

    */
	function maximalizeInterface()
	{
		var minImg = document.getElementById('minimalizeInterfaceIMG');
		var maxImg = document.getElementById('maximalizeInterfaceIMG');
		var menu = document.getElementById('interfejsmenu');
		if(menu && maxImg && minImg){
			menu.style.display = '';
			minImg.style.display = '';
			maxImg.style.display = 'none';
			isInterfaceMinimalize = 0;
			ajax4.requestFile = "https:\/\/synergia.librus.pl" + '/ajax/stan_minimalizacji_interfejsu.php?isInterfaceMin='+isInterfaceMinimalize;
			ajax4.runAJAX();
		}
	}
    
    /*
      Class: lessonBarStatus

      Description:
      LessonBar object with refresh functionality
      
      Author:
      Krzysztof Żerucha <krzysztof.zerucha[at]librus.pl>
    */
    
    var lessonBarStatus = function(lessonStart, lessonEnd, now, lessonNumber)
    {
        if (!Number.isInteger(lessonStart) || lessonStart < 0) {
            throw new Error("Wrong type of input variable 'lessonStart'. Expected non-negative integer.");
        }

        if (!Number.isInteger(lessonEnd) || lessonEnd < 0) {
            throw new Error("Wrong type of input variable 'lessonEnd'. Expected non-negative integer.");
        }
        
        if (lessonStart >= lessonEnd) {
            throw new Error("Wrong value of 'lessonStart' or 'lessonEnd'. 'lessonStar' is later than 'lessonEnd'.");
        }

        if (!Number.isInteger(now) || now < 0) {
            throw new Error("Wrong type of input variable 'now'. Expected non-negative integer.");
        }

        if ((!Number.isInteger(lessonNumber) || lessonNumber < 0) && lessonNumber !== null) {
            throw new Error("Wrong type of input variable 'lessonNumber'. Expected non-negative integer or null.");
        }
        
        if (document.getElementById('progressBarLekcja') === null) {
            throw new Error("progressBarLekcja not found");
        }
        
        if (document.getElementById('progressBarText') === null) {
            throw new Error("progressBarText not found");
        }
        
        var lessonStart = lessonStart;
        var lessonEnd = lessonEnd;
        var actualTimeForLessonBar = now;
        var lessonNumber = lessonNumber;
        var divBar = document.getElementById('progressBarLekcja');
        var divTxtNr = document.getElementById('progressBarText');
        var lessonBarInterval;
        this.refresh = function() {
            var actualTime = actualTimeForLessonBar;
            actualTimeForLessonBar += 60;
            var percent = 0;
            var remainingMinutes = 0;
            if (lessonNumber !== null) {
                percent = 100;
                if (actualTime <= lessonEnd) {
                    var percent = (actualTime - lessonStart) / (lessonEnd - lessonStart) * 100;
                    var remainingMinutes = parseInt((lessonEnd - actualTime) / 60);
                }
                
                divBar.style.width = percent + '%';
                divTxtNr.innerHTML = 'Do końca lek. nr ' + lessonNumber + ': <span class="progressBarTextMinutes">' + remainingMinutes + ' min</span>';
            } else {
                divTxtNr.innerHTML = 'Do końca lekcji';
                this.stopInterval();
            }

            if (percent >= 100) {
                this.stopInterval();
            }
        }
    };
    
    lessonBarStatus.prototype.startInterval = function() {
        var self = this;
        lessonBarInterval = window.setInterval(function() {self.refresh();}, 60000);
        self.refresh();
    };
    
    lessonBarStatus.prototype.stopInterval = function() {
        clearInterval(lessonBarInterval);
    };

	function ustaw_komunikat_menu(stan)
	{
		ajaxKomMenu.requestFile = "https:\/\/synergia.librus.pl" + '/ajax/ustaw_komunikat_menu.php?stan='+stan;
		ajaxKomMenu.execute = true;
		ajaxKomMenu.runAJAX();

	}

    function ustaw_komunikat_archiwum(stan)
	{
		ajaxKomMenu.requestFile = "https:\/\/synergia.librus.pl" + '/ajax/ustaw_komunikat_archiwum.php?stan='+stan;
		ajaxKomMenu.execute = true;
		ajaxKomMenu.runAJAX();

	}

	function zmien_typ_menu(stan)
	{
            $.ajax({
                dataType: "json",
                type: "post",
                url: '/ajax/zmien_typ_menu_adv.php',
                data: 'stan='+stan,
                success: function(data){
                    if( data["SUCCESS"] ) {
                        var html = $.parseHTML( data["CONTENT"], document, true );
                        var scripts = [];
                        var found = null;
                        $.each( html, function( i, el ) {
                            if(el.nodeName == 'SCRIPT') {
                                scripts[i] = el;
                            } else if(el.nodeName != '#text') {
                                if($(el).attr("id") == "header") {
                                    found = $(el);
                                }
                            }
                        });
                        if (found.length > 0) {
                            $("#header").replaceWith(found);
                            setTimeout(function(){
                                $.each( scripts, function( i, el ) {
                                    $("#header").append(el);
                                });
                            }, 500);
                        }
                    }
                }
            });
	}

    var interfejs_fold_object = function()
    {
        var res = new Object();
        $('#interfejs-lekcyjny div[id^="g_"]').each(function() {
            res[$(this).attr('id')] = !$(this).is(':visible');
        });

        return res;
    };

//------------

//-->
 </script>



 <script type="text/javascript" src="/js/ImageSwaper.js"></script>


<script type="text/javascript" charset="UTF-8">
/* <![CDATA[ */
try { if (undefined == xajax.config) xajax.config = {}; } catch (e) { xajax = {}; xajax.config = {}; };
xajax.config.requestURI = "https://synergia.librus.pl/przegladaj_oceny/uczen?f=3";
xajax.config.statusMessages = false;
xajax.config.waitCursor = true;
xajax.config.version = "xajax 0.5 rc2";
xajax.config.legacy = false;
xajax.config.defaultMode = "asynchronous";
xajax.config.defaultMethod = "POST";
/* ]]> */
</script>
<script type="text/javascript" src="/funkcje/xajax/xajax_js/xajax_core.js" charset="UTF-8"></script>
<script type="text/javascript" charset="UTF-8">
/* <![CDATA[ */
window.setTimeout(
 function() {
  var scriptExists = false;
  try { if (xajax.isLoaded) scriptExists = true; }
  catch (e) {}
  if (!scriptExists) {
   alert("Error: the xajax Javascript component could not be included. Perhaps the URL is incorrect?\nURL: /funkcje/xajax/xajax_js/xajax_core.js");
  }
 }, 2000);
/* ]]> */
</script>

<script type='text/javascript' charset='UTF-8'>
/* <![CDATA[ */
xajax_pobierzArtykulPomocy = function() { return xajax.request( { xjxfun: 'pobierzArtykulPomocy' }, { parameters: arguments } ); };
xajax_pobierzDzialPomocy = function() { return xajax.request( { xjxfun: 'pobierzDzialPomocy' }, { parameters: arguments } ); };
xajax_ustaw_komunikat_menu = function() { return xajax.request( { xjxfun: 'ustaw_komunikat_menu' }, { parameters: arguments } ); };
xajax_ustaw_komunikat_archiwum = function() { return xajax.request( { xjxfun: 'ustaw_komunikat_archiwum' }, { parameters: arguments } ); };
xajax_zmien_typ_menu = function() { return xajax.request( { xjxfun: 'zmien_typ_menu' }, { parameters: arguments } ); };
/* ]]> */
</script>
        <script type="text/javascript">
            if (xajax) {
                xajax.config.postHeaders["requestkey"] = "MC41MDE0MzgwMCAxNTU2NDU4NDY4XzdiYzcyNmE1YTdlZDQ3NmE0YTM4Y2EwYjk0N2Y5MmJk";
            }
        </script>
        </head>
<body onload="ImageSwaper.GetInstance().Load(); ">

<script type="text/javascript">
  var toload = 0;  
  var imageShowed = false;
    
  /*
       Function: startLoading

       Function show preloader.

       Author:
            Sylwester Wydmuch <sylwester.wydmuch[at]librus.pl>

    */
  function startLoading()
  {
    toload = toload + 1; 
    window.setTimeout('showLoadingImage()', 700);
    
  }
    
  /*
       Function: showLoadingImage

       Function show preloader image.

       Author:
            Sylwester Wydmuch <sylwester.wydmuch[at]librus.pl>

    */
  function showLoadingImage()
  {
   if(!imageShowed && toload>0) 
   {
    if (document.getElementById('loading')) {
        document.getElementById('loading').style.display = 'block';
        imageShowed=true;
    }	
   }
  }
    
  /*
       Function: stopLoading

       Function hide preloader.

       Author:
            Sylwester Wydmuch <sylwester.wydmuch[at]librus.pl>

    */
  function stopLoading() 
  {
    toload = toload -1;
    if(toload==0) 
  	{
  		document.getElementById('preloader-box').style.display='none';
  		imageShowed = false;
        $( 'input[type=submit],input[type=button], button' ).not('.ui-button').not('.advanced-button').css({'z-index': $(this).parent().zIndex() }).button();
        

        $( '.button-set .advanced-button' ).not('.ui-button').button({
            text: true,
            icons: {
                secondary: 'ui-icon-triangle-1-s'
            }
        }).click(function() {
            $('.button-set ul').not( $( this ).parent().next() ).hide();
            var menu = $( this ).parent().next().stop().animate({
                height: 'toggle'
            }, 100);
            $( document ).one( 'click', function() {
                menu.hide();
            });
            return false;
        }).parent().next().hide().css({'z-index': $(this).parent().zIndex()+1}).menu().buttonset();
	}
    
  }
  xajax.callback.global.onRequest = function() {startLoading();}
  xajax.callback.global.onComplete = function() {stopLoading();}
  </script><div id="preloader-box"  style="display: none;">
            <div id="preloader-curtain"></div>
            <div id="preloader">
                <span id="preloader-logo"></span>
                <span id="preloader-loader"></span>
            </div>
        </div>    <div class="yellowCard  maximalize " class="screen-only ui-widget-content draggable"
                id="yellowCard_1"
        style=" left: 0px; top: 0px; display: none;"
    >
        <div class="titleYellowCard  maximalize " id="yellowCard_1_DraggHandle"
            onmouseup="interface_object_save_position('yellowCard_1', $(this.parentNode).offset().left, $(this.parentNode).offset().top, interfejs_fold_object());"
        >
            <div class="content"></div>
        </div>
        <div class="yellowCardClose" onmouseup="return false;" onclick='yellowCardAction("close", "yellowCard_1");'>
            <a href="javascript:void(0);"></a>
        </div>
                        <div class="yellowCardMinimalize" onclick='yellowCardAction("minimalize", "yellowCard_1");' ></div>
                <div class="yellowCardMaximalize" onclick='yellowCardAction("maximalize", "yellowCard_1");' style="display:none;" ></div>
                     <div class="cardBody" >
            <div class="content"></div>
        </div>
    </div>
    <script type="text/javascript">$('#yellowCard_1').offset({ top: screen.height/2-150, left: screen.width/2-150});</script>
<script type="text/javascript" src="/wz_dragdrop.js"></script>
 <!--
 <div id="kontener">
 <div id="baner"><img src="/images/baner_top1.jpg"></div>
  -->
    <div id="page" class="altum">
        <div id="header">    <div id="main-navigation-container">
        <div id="main-navigation">            <div id="main-menu">
                <ul class="main-menu-list">
                    <li><a href="/wyloguj">Wyloguj</a></li>                    <li><a href="/help" target="_blank">Pomoc</a></li>                    <li><a href="/ustawienia">Ustawienia</a></li>                    <li><a href="/ankiety_admin_ankiety">Ankiety</a></li>
                    <li>
                        <a href="javascript: void(null);">Uczeń</a>
                        <ul>                            <li><a href="/uwagi">Uwagi</a></li>                            <li><a href="/uczen_wyniki_egzaminow">Wyniki egzaminów</a></li>                            <li><a href="/szczegolne_osiagniecia_ucznia">Szczególne osiągnięcia</a></li>                            <li><a href="/informacja">Informacje</a></li>                        </ul>
                    </li>                    <li>
                        <a href="javascript: void(null);">Organizacja</a>
                        <ul>
                            <li><a href="javascript:otworz_w_nowym_oknie('/przegladaj_plan_lekcji','plan_u',0,0)">Plan lekcji</a></li>
                            <li><a href="/dyzury">Dyżury</a></li>                            <li><a href="/zrealizowane_lekcje">Lekcje</a></li>                            <li><a href="/pliki_szkoly">Pliki&nbsp;szkoły</a></li>                        </ul>
                    </li>                </ul>
            </div>
        </div>
    </div>    <div id="top-banner-container">
        <a href="/rodzic/index             "             >
            <img id="top-banner" src="/assets/img/synergia/header/benners/default.png         " />
        </a>        <div id="graphic-menu">
            <ul>                    <li><a title="Liczba ocen dodanych od ostatniego logowania: 2"         href="/przegladaj_oceny/uczen" id="icon-oceny"><span class="circle"></span>Oceny</a>
                            <form action="/przegladaj_oceny/uczen" method="POST" id="liczba_ocen_od_ostatniego_logowania_form" class="hidden">
                                <input type="hidden" name="zmiany_logowanie" value="zmiany_logowanie" />
                            </form>
                            <a href="javascript:$('#liczba_ocen_od_ostatniego_logowania_form').submit();" class="button counter blue">2         </a>
                    </li>
                    <li>
                        <a          href="/przegladaj_nb/uczen" id="icon-nb">
                            <span class="circle"></span>Frekwencja
                        </a>
                    </li>
                <li><a  title="Liczba nieprzeczytanych wiadomości: 7"         href="/wiadomosci             "         id="icon-wiadomosci"><span class="circle"></span>Wiadomości</a>                    <a class="button counter">7         </a>
                <li><a title="Liczba nieprzeczytanych ogłoszeń: 6"         href="/ogloszenia" id="icon-ogloszenia"><span class="circle"></span>Ogłoszenia</a>                    <a class="button counter">6         </a>
                        </li>
                <li><a          href="/terminarz" id="icon-terminarz"><span class="circle"></span>Terminarz</a>                            </li>
                    <li><a          href="/moje_zadania" id="icon-zadania"><span class="circle"></span>Zadania domowe</a>                    </li>
            </ul>
        </div>    </div>
    <div id="user-section">
szczęśliwy numerek: <span class="luckyNumber">&nbsp;<b>17</b>&nbsp;</span> &nbsp;&nbsp; <span class="tooltip" title="&lt;b&gt;ostatnie udane logowania:&lt;/b&gt;&lt;br /&gt;2019-04-28 15:34:28, IP: 89.66.175.34&lt;br /&gt;2019-04-28 15:34:26, IP: 89.66.175.34&lt;br /&gt;2019-04-25 16:02:36, IP: 89.66.175.34&lt;br /&gt;2019-04-25 16:02:34, IP: 89.66.175.34&lt;br /&gt;2019-04-24 20:39:00, IP: 89.66.175.34&lt;br /&gt;&lt;b&gt;ostatnie nieudane logowania:&lt;/b&gt;&lt;br /&gt;brak">historia logowań </span>&nbsp;<img class="tooltip helper-icon" alt="&lt;b&gt;ostatnie udane logowania:&lt;/b&gt;&lt;br /&gt;2019-04-28 15:34:28, IP: 89.66.175.34&lt;br /&gt;2019-04-28 15:34:26, IP: 89.66.175.34&lt;br /&gt;2019-04-25 16:02:36, IP: 89.66.175.34&lt;br /&gt;2019-04-25 16:02:34, IP: 89.66.175.34&lt;br /&gt;2019-04-24 20:39:00, IP: 89.66.175.34&lt;br /&gt;&lt;b&gt;ostatnie nieudane logowania:&lt;/b&gt;&lt;br /&gt;brak" title="&lt;b&gt;ostatnie udane logowania:&lt;/b&gt;&lt;br /&gt;2019-04-28 15:34:28, IP: 89.66.175.34&lt;br /&gt;2019-04-28 15:34:26, IP: 89.66.175.34&lt;br /&gt;2019-04-25 16:02:36, IP: 89.66.175.34&lt;br /&gt;2019-04-25 16:02:34, IP: 89.66.175.34&lt;br /&gt;2019-04-24 20:39:00, IP: 89.66.175.34&lt;br /&gt;&lt;b&gt;ostatnie nieudane logowania:&lt;/b&gt;&lt;br /&gt;brak" src="/images/pomoc_ciemna.png" />&nbsp; | jesteś zalogowany jako: <b>
Aleksander Iwicki&nbsp;(rodzic&nbsp;<img class="tooltip helper-icon" alt="Dane można zmienić w konfiguracji" title="Dane można zmienić w konfiguracji" src="/images/pomoc_ciemna.png" />&nbsp;)</b>    </div>
</div>

<script type="text/javascript">
    function zapiszPaddingStrony( padding ) {
        $.ajax({
            type: "POST",
            url: '/ajax/setPagePadding.php',
            data: 'enlarge='+padding,
            success: function() {
            },
            error: function() {
            }
          });
    }
        $(document).ready(function() {
        if( window.width <= 1024 ) {
            zapiszPaddingStrony(1);
            $("#body").css("padding-left","0");
            $("#body").css("padding-right","0");
        }
    });
        </script>
<div id="left-widgets-container">
    </div>
<div id="body"  >
    <table class="decorated stretch" style="display: none;">
        <tr class="line0">
            <td><img id="przedmioty_00000_node" onclick=""></td>
            <td></td>
            <td class="line0">
                <span id="Ocena0" class="grade-box">
                    <a title="" class="ocena" href="/przegladaj_oceny/szczegoly/000000" aria-describedby="ui-tooltip-1" id="ocenaTest">1</a>
                </span>
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr class="line0" id="przedmioty_00000">
            <td colspan="12">
                <table class="stretch">
                    <tbody>
                        <tr class="line1 detail-grades">
                            <td class="center">1</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        $(document).ready(function() {
                
                        setTimeout(function() {
                if($('#ocenaTest').html() != "1") {
                    show_message(
                        'error medium', 
                        'Uwaga!', 
                        'System wykrył w przeglądarce Twojego komputera niewłaściwe <b>rozszerzenie, które błędnie wyświetla oceny Twojego Dziecka</b>.<br/>Informujemy, że <b>oceny które wprowadzili Nauczyciele do systemu w rzeczywistości nie zostały zmienione</b>.<br/><br/>Usuń rozszerzenie z ustawień przeglądarki, a następnie ponownie zaloguj się do rozwiązania LIBRUS Synergia.<br/><br/>Więcej informacji na temat rozszerzenia znajdziesz <a target="_blank" href="https://chrome.google.com/webstore/detail/librusowy-asystent-ucznia/ilnfpopncflimblljcnjekjlkcpnkpdg?hl=pl">tutaj</a>.', 
                        [], 
                        true
                    );
                }
            }, 2000);

        });
    </script>
                <script type="text/javascript" src="/js/Blinker.js"></script>
            <script type="text/javascript">
                /*
                Variable: blinker
                [object] blinker object.
                 */
                var blinker = new Blinker();
                /*
                Variable: blinkerOldOnLoad
                [object] blinker onload object.
                 */
                var blinkerOldOnLoad = window.onload;
                window.onload = function() {
                    blinker.Start();
                    if(blinkerOldOnLoad) {
                        blinkerOldOnLoad();
                    }
                };
            </script>
            <FORM ACTION="/przegladaj_oceny/uczen" METHOD="POST" NAME="PrzegladajOceny">    
    <input 
        type="hidden" 
        name="requestkey" 
        value="MC41MDE0MzgwMCAxNTU2NDU4NDY4XzdiYzcyNmE1YTdlZDQ3NmE0YTM4Y2EwYjk0N2Y5MmJk" 
    />

                <div class="container">
                <h2 class="inside">Oceny ucznia
                                </h2>
                <span class="fold">    <a href="javascript:print();" class="fold-link"><span class="fold-start">Drukuj</span><span class="fold-end"></span></a></span><input type="hidden" disabled="disabled" name="zmiany_logowanie" id="zmiany_logowanie"  /><span class="fold">    <a href="javascript:
                    $(&#039;input[name=zmiany_logowanie_wszystkie]&#039;).remove();
                    $(&#039;input[name=zmiany_logowanie_tydzien]&#039;).remove();
                    $(&#039;&lt;input&gt;&#039;).attr({ type: &#039;hidden&#039;,id: &#039;zmiany_logowanie&#039;,name: &#039;zmiany_logowanie&#039;, value : &#039;1&#039;}).appendTo(&#039;form&#039;);
                    document.forms[&#039;PrzegladajOceny&#039;].submit();" class="fold-link"><span class="fold-start">Pokaż dodane od ostatniego logowania</span><span class="fold-end"></span></a></span><input type="hidden" disabled="disabled" name="zmiany_logowanie_tydzien" id="zmiany_logowanie_tydzien"  /><span class="fold">    <a href="javascript:
                    $(&#039;input[name=zmiany_logowanie]&#039;).remove();
                    $(&#039;input[name=zmiany_logowanie_wszystkie]&#039;).remove();
                    $(&#039;&lt;input&gt;&#039;).attr({ type: &#039;hidden&#039;,id: &#039;zmiany_logowanie_tydzien&#039;,name: &#039;zmiany_logowanie_tydzien&#039;, value : &#039;1&#039;}).appendTo(&#039;form&#039;);
                    document.forms[&#039;PrzegladajOceny&#039;].submit();" class="fold-link"><span class="fold-start">Pokaż dodane w aktualnym tygodniu</span><span class="fold-end"></span></a></span><span class="fold">    <a href="/archiwum" class="fold-link"><span class="fold-start">Archiwum</span><span class="fold-end"></span></a></span><div class="container-background">
    <div class="container-icon">
        <table>
            <tbody>
                <tr>
                    <td>
                        <img src="/images/naglowek_uczen.png" alt="Uczeń" class="screen-only" />
                    </td>       <td><p><b>Uczeń:</b> Iwicki Aleksander<br /><b>Klasa: </b>3B GIM&nbsp;</p></td>
                </tr>
            </tbody>
        </table>
    </div><div ><center></center></div>    <script src="/js/cookie.js" type="text/javascript"></script>
	<script src="/js/showHideRows.js" type="text/javascript"></script>
	<script type="text/javascript">
        /*
           Variable: allUids
           [array] table of subject identifiers
          */
		var allUids = Array('zachowanie', 32775, 32776, 32793, 32777, 32778, 32780, 32781, 32782, 32783, 32784, 32785, 32788, 32790, 32791, 32792, 34012, 75173);
		var showHide = new ShowHideRows('przedmioty_', 'przedmioty_27005', allUids, null);

		var old_on_load = window.onload;
		window.onload = function()
		{
			showHide.Restore();

			if(old_on_load) {
				old_on_load();
			}
		};
	</script>
	<br /><h3 class="center">Oceny bieżące</h3><div class='right screen-only'><table><tr><td class="right"><table class="right sort_box"><tr><td>sortuj </td><td><select name="sortowanieOcen" class="right black" onChange='this.form.submit()'><option value="2" selected>wg daty</option><option value="1" >wg kategorii</option><option value="3" >wg ocen</option></select></td></tr></table></td></tr></table></div><table class="decorated stretch">
   <thead>
   <tr>
   	<th  class="micro screen-only" rowspan="2" ><img src="/images/tree_colapsed.png" id="przedmioty_all_node" onclick="showHide.AllNodeHandler();" /></th>
   	<td rowspan="2" >Przedmiot</td>
         <td colspan="4" class="colspan center"><span>Okres 1</span></td>
         <td colspan="3" class="colspan center"><span>Okres 2</span></td>
         <td colspan="3"  class="colspan center"><span>Koniec roku</span></td></tr>
       <tr>
        <td class="no-border-top spacing">Oceny bieżące</td><td class="no-border-top no-border-left" title="Średnia ocen<br> z pierwszego okresu">Śr.I</td><td  class="no-border-top" title="Przewidywana ocena śródroczna<br> z pierwszego okresu">(I)</td><td  class="no-border-top" title="Ocena śródroczna z pierwszego okresu">I</td>
        <td  class="no-border-top">Oceny bieżące</td><td  class="no-border-top" title="Średnia ocen z drugiego okresu">Śr.II</td>  <td  class="no-border-top" title="Ocena śródroczna z drugiego okresu">II</td><td  class="no-border-top" title="Średnia roczna">Śr.R</td> <td  class="no-border-top" title="Przewidywana ocena roczna">(R)</td> <td  class="no-border-top"  title="Ocena roczna">R</td></tr></thead><tr class="line0">
                            <td class='center micro screen-only'><img src="/images/tree_colapsed.png" id="przedmioty_32775_node" onclick="showHide.ShowHide(32775);" /></td>
                            <td >Biologia</td><td ><span id="Ocena1" class="grade-box" style="background-color:#3333FF; ">
            <a title="Kategoria: mały sprawdzian<br>Data: 2018-10-11 (czw.)<br>Nauczyciel: Niemczyk Katarzyna<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Niemczyk Katarzyna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/4230868" >4+</a></span><span id="Ocena2" class="grade-box" style="background-color:#3333FF; ">
            <a title="Kategoria: mały sprawdzian<br>Data: 2018-10-18 (czw.)<br>Nauczyciel: Niemczyk Katarzyna<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Niemczyk Katarzyna<br/><br>Komentarz: układ immunologiczny" class="ocena" href="/przegladaj_oceny/szczegoly/5320215" >4+</a></span><span id="Ocena3" class="grade-box" style="background-color:#DC143C; ">
            <a title="Kategoria: praca klasowa<br>Data: 2018-11-21 (śr.)<br>Nauczyciel: Niemczyk Katarzyna<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Niemczyk Katarzyna<br/>Obowiązek wyk. zadania: TAK<br/><br>Komentarz: układ nerwowy,wydalniczy" class="ocena" href="/przegladaj_oceny/szczegoly/10611532" >4</a></span><span id="Ocena4" class="grade-box" style="background-color:#DC143C; ">
            <a title="Kategoria: praca klasowa<br>Data: 2018-12-18 (wt.)<br>Nauczyciel: Niemczyk Katarzyna<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Niemczyk Katarzyna<br/>Obowiązek wyk. zadania: TAK<br/><br>Komentarz: układ hormonalny,zmysły,skóra" class="ocena" href="/przegladaj_oceny/szczegoly/16047380" >4</a></span><span id="Ocena5" class="grade-box" style="background-color:#3333FF; ">
            <a title="Kategoria: mały sprawdzian<br>Data: 2019-01-21 (pon.)<br>Nauczyciel: Niemczyk Katarzyna<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Niemczyk Katarzyna<br/><br>Komentarz: układ rozrodczy" class="ocena" href="/przegladaj_oceny/szczegoly/21667304" >4+</a></span></td><td class="right">4.25</td><td class="center" > - </td><td class="center" ><span id="Ocena6" class="grade-box" style="background-color:#B0C4DE; ">
            <a title="Kategoria: śródroczna<br>Data: 2019-01-21 (pon.)<br>Nauczyciel: Niemczyk Katarzyna<br>Licz do średniej: nie<br>Dodał: Niemczyk Katarzyna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/21668130" >4</a></span></td><td ><span id="Ocena7" class="grade-box" style="background-color:#DC143C; ">
            <a title="Kategoria: praca klasowa<br>Data: 2019-03-01 (pt.)<br>Nauczyciel: Niemczyk Katarzyna<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Niemczyk Katarzyna<br/>Obowiązek wyk. zadania: TAK<br/><br>Komentarz: genetyka" class="ocena" href="/przegladaj_oceny/szczegoly/26000872" >4+</a></span><span id="Ocena8" class="grade-box" style="background-color:#DC143C; ">
            <a title="Kategoria: praca klasowa<br>Data: 2019-04-17 (śr.)<br>Nauczyciel: Niemczyk Katarzyna<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Niemczyk Katarzyna<br/>Obowiązek wyk. zadania: TAK<br/><br>Komentarz: praca klasowa" class="ocena" href="/przegladaj_oceny/szczegoly/32512305" >4</a></span></td><td  class="right">4.25</td><td class="center"  > - </td><td  class="right"  >4.25</td><td class="center"  ><span id="Ocena9" class="grade-box" style="background-color:#F0FFFF; ">
            <a title="Kategoria: przewidywana roczna<br>Data: 2019-04-28 (nd.)<br>Nauczyciel: Niemczyk Katarzyna<br>Licz do średniej: nie<br>Dodał: Niemczyk Katarzyna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/32743862" >4</a></span></td><td class="center"  > - </td></tr><tr class="line0" name="przedmioty_all" id="przedmioty_32775" style="display: none; visibility: hidden;">
                              <td colspan="14" ><table class="stretch"><thead><tr class=""><td >Ocena</td><td title="Komentarz do oceny">K</td><td>Kategoria</td><td class="center">Data</td><td>Nauczyciel</td><td>Licz do śr.</td><td>Waga</td><td >Poprawa oceny</td><td>Dodał</td></tr></thead><tbody><tr class="line1 bolded"><td colspan="9" class="center">Okres 1</td></tr><tr class="line1 detail-grades" style="background-color: #3333FF;"><td class="center">4+</td><td class="center">&nbsp;</td><td>mały sprawdzian</td><td class="center">2018-10-11</td><td >Niemczyk Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Niemczyk Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #3333FF;"><td class="center">4+</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/5320215','ko',480,450)" >K</a></strong></td><td>mały sprawdzian</td><td class="center">2018-10-18</td><td >Niemczyk Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Niemczyk Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #DC143C;"><td class="center">4</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/10611532','ko',480,450)" >K</a></strong></td><td>praca klasowa</td><td class="center">2018-11-21</td><td >Niemczyk Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Niemczyk Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #DC143C;"><td class="center">4</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/16047380','ko',480,450)" >K</a></strong></td><td>praca klasowa</td><td class="center">2018-12-18</td><td >Niemczyk Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Niemczyk Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #B0C4DE;"><td class="center">4</td><td class="center">&nbsp;</td><td>śródroczna</td><td class="center">2019-01-21</td><td >Niemczyk Katarzyna</td><td class="center"><img src="/images/nieaktywne.png"  border="0" /></td><td class="right" ></td><td class="center" >-</td><td >Niemczyk Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #3333FF;"><td class="center">4+</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/21667304','ko',480,450)" >K</a></strong></td><td>mały sprawdzian</td><td class="center">2019-01-21</td><td >Niemczyk Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Niemczyk Katarzyna</td></tr><tr class="line1 bolded"><td colspan="9" class="center">Okres 2</td></tr><tr class="line1 detail-grades" style="background-color: #DC143C;"><td class="center">4+</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/26000872','ko',480,450)" >K</a></strong></td><td>praca klasowa</td><td class="center">2019-03-01</td><td >Niemczyk Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Niemczyk Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #DC143C;"><td class="center">4</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/32512305','ko',480,450)" >K</a></strong></td><td>praca klasowa</td><td class="center">2019-04-17</td><td >Niemczyk Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Niemczyk Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #F0FFFF;"><td class="center">4</td><td class="center">&nbsp;</td><td>przewidywana roczna</td><td class="center">2019-04-28</td><td >Niemczyk Katarzyna</td><td class="center"><img src="/images/nieaktywne.png"  border="0" /></td><td class="right" ></td><td class="center" >-</td><td >Niemczyk Katarzyna</td></tr></tbody><tfoot><tr><td  class="center" colspan="14"></td></tr></tfoot></table></td></tr><tr class="line1">
                            <td class='center micro screen-only'><img src="/images/tree_colapsed.png" id="przedmioty_32776_node" onclick="showHide.ShowHide(32776);" /></td>
                            <td >Chemia</td><td ><span id="Ocena10" class="grade-box" style="background-color:#BA55D3; ">
            <a title="Kategoria: Kartkówka<br>Data: 2018-10-17 (śr.)<br>Nauczyciel: Tomczyk Andżelika<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Tomczyk Andżelika<br/><br>Komentarz: Alkany" class="ocena" href="/przegladaj_oceny/szczegoly/5165134" >6</a></span><span id="Ocena11" class="grade-box" style="background-color:#66CDAA; ">
            <a title="Kategoria: Sprawdzian<br>Data: 2018-10-18 (czw.)<br>Nauczyciel: Tomczyk Andżelika<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Tomczyk Andżelika<br/><br>Komentarz: sole<br />
" class="ocena" href="/przegladaj_oceny/szczegoly/5276043" >3</a></span><span id="Ocena12" class="grade-box" style="background-color:#FFD700; ">
            <a title="Kategoria: referat<br>Data: 2018-11-07 (śr.)<br>Nauczyciel: Tomczyk Andżelika<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Tomczyk Andżelika<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/8289443" >6</a></span><span id="Ocena13" class="grade-box" style="background-color:#FF1493; ">
            <a title="Kategoria: praca klasowa<br>Data: 2018-11-21 (śr.)<br>Nauczyciel: Tomczyk Andżelika<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Tomczyk Andżelika<br/><br>Komentarz: węglowodory" class="ocena" href="/przegladaj_oceny/szczegoly/10497976" >4+</a></span><span id="Ocena14" class="grade-box" style="background-color:#ADFF2F; ">
            <a title="Ocena: nieprzygotowany<br>Kategoria: aktywność<br>Data: 2018-11-28 (śr.)<br>Nauczyciel: Tomczyk Andżelika<br>Dodał: Tomczyk Andżelika<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/11668415" >np</a></span><span id="Ocena15" class="grade-box" style="background-color:#ADFF2F; ">
            <a title="Kategoria: aktywność<br>Data: 2018-12-12 (śr.)<br>Nauczyciel: Tomczyk Andżelika<br>Dodał: Tomczyk Andżelika<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/14516205" >-</a></span><span id="Ocena16" class="grade-box" style="background-color:#FF1493; ">
            <a title="Kategoria: praca klasowa<br>Data: 2019-01-11 (pt.)<br>Nauczyciel: Tomczyk Andżelika<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Tomczyk Andżelika<br/><br>Komentarz: Kwasy karboksylowe i alkohole" class="ocena" href="/przegladaj_oceny/szczegoly/19976492" >3</a></span></td><td class="right">4.05</td><td class="center" > - </td><td class="center" ><span id="Ocena17" class="grade-box" style="background-color:#B0C4DE; ">
            <a title="Kategoria: śródroczna<br>Data: 2019-01-23 (śr.)<br>Nauczyciel: Tomczyk Andżelika<br>Licz do średniej: nie<br>Dodał: Tomczyk Andżelika<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/21932290" >4</a></span></td><td ><span id="Ocena18" class="grade-box" style="background-color:#66CDAA; ">
            <a title="Kategoria: Sprawdzian<br>Data: 2019-02-07 (czw.)<br>Nauczyciel: Tomczyk Andżelika<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Tomczyk Andżelika<br/><br>Komentarz: aminy, estry i aminokwasy" class="ocena" href="/przegladaj_oceny/szczegoly/23321591" >5+</a></span><span id="Ocena19" class="grade-box" style="background-color:#FF1493; ">
            <a title="Kategoria: praca klasowa<br>Data: 2019-03-21 (czw.)<br>Nauczyciel: Tomczyk Andżelika<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Tomczyk Andżelika<br/><br>Komentarz: tłuszcze" class="ocena" href="/przegladaj_oceny/szczegoly/29442250" >5</a></span></td><td  class="right">5.20</td><td class="center"  > - </td><td  class="right"  >4.43</td><td class="center"  > - </td><td class="center"  > - </td></tr><tr class="line1" name="przedmioty_all" id="przedmioty_32776" style="display: none; visibility: hidden;">
                              <td colspan="14" ><table class="stretch"><thead><tr class=""><td >Ocena</td><td title="Komentarz do oceny">K</td><td>Kategoria</td><td class="center">Data</td><td>Nauczyciel</td><td>Licz do śr.</td><td>Waga</td><td >Poprawa oceny</td><td>Dodał</td></tr></thead><tbody><tr class="line1 bolded"><td colspan="9" class="center">Okres 1</td></tr><tr class="line1 detail-grades" style="background-color: #BA55D3;"><td class="center">6</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/5165134','ko',480,450)" >K</a></strong></td><td>Kartkówka</td><td class="center">2018-10-17</td><td >Tomczyk Andżelika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Tomczyk Andżelika</td></tr><tr class="line1 detail-grades" style="background-color: #66CDAA;"><td class="center">3</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/5276043','ko',480,450)" >K</a></strong></td><td>Sprawdzian</td><td class="center">2018-10-18</td><td >Tomczyk Andżelika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Tomczyk Andżelika</td></tr><tr class="line1 detail-grades" style="background-color: #FFD700;"><td class="center">6</td><td class="center">&nbsp;</td><td>referat</td><td class="center">2018-11-07</td><td >Tomczyk Andżelika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Tomczyk Andżelika</td></tr><tr class="line1 detail-grades" style="background-color: #FF1493;"><td class="center">4+</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/10497976','ko',480,450)" >K</a></strong></td><td>praca klasowa</td><td class="center">2018-11-21</td><td >Tomczyk Andżelika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Tomczyk Andżelika</td></tr><tr class="line1 detail-grades" style="background-color: #ADFF2F;"><td class="center">np</td><td class="center">&nbsp;</td><td>aktywność</td><td class="center">2018-11-28</td><td >Tomczyk Andżelika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Tomczyk Andżelika</td></tr><tr class="line1 detail-grades" style="background-color: #ADFF2F;"><td class="center">-</td><td class="center">&nbsp;</td><td>aktywność</td><td class="center">2018-12-12</td><td >Tomczyk Andżelika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Tomczyk Andżelika</td></tr><tr class="line1 detail-grades" style="background-color: #FF1493;"><td class="center">3</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/19976492','ko',480,450)" >K</a></strong></td><td>praca klasowa</td><td class="center">2019-01-11</td><td >Tomczyk Andżelika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Tomczyk Andżelika</td></tr><tr class="line1 detail-grades" style="background-color: #B0C4DE;"><td class="center">4</td><td class="center">&nbsp;</td><td>śródroczna</td><td class="center">2019-01-23</td><td >Tomczyk Andżelika</td><td class="center"><img src="/images/nieaktywne.png"  border="0" /></td><td class="right" ></td><td class="center" >-</td><td >Tomczyk Andżelika</td></tr><tr class="line1 bolded"><td colspan="9" class="center">Okres 2</td></tr><tr class="line1 detail-grades" style="background-color: #66CDAA;"><td class="center">5+</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/23321591','ko',480,450)" >K</a></strong></td><td>Sprawdzian</td><td class="center">2019-02-07</td><td >Tomczyk Andżelika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Tomczyk Andżelika</td></tr><tr class="line1 detail-grades" style="background-color: #FF1493;"><td class="center">5</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/29442250','ko',480,450)" >K</a></strong></td><td>praca klasowa</td><td class="center">2019-03-21</td><td >Tomczyk Andżelika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Tomczyk Andżelika</td></tr></tbody><tfoot><tr><td  class="center" colspan="14"></td></tr></tfoot></table></td></tr><tr class="line0">
                            <td class='center micro screen-only'><img src="/images/tree_colapsed.png" id="przedmioty_32793_node" onclick="showHide.ShowHide(32793);" /></td>
                            <td >Edukacja dla bezpieczeństwa</td><td ><span id="Ocena20" class="grade-box" style="background-color:#F0E68C; ">
            <a title="Kategoria: dz.1<br>Data: 2018-10-02 (wt.)<br>Nauczyciel: Jaworski Maciej<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Jaworski Maciej<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/2526581" >6</a></span><span id="Ocena21" class="grade-box" style="background-color:#F0E68C; ">
            <a title="Kategoria: dz 3<br>Data: 2018-11-20 (wt.)<br>Nauczyciel: Jaworski Maciej<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Jaworski Maciej<br/><br>Komentarz: spr. dz. 3" class="ocena" href="/przegladaj_oceny/szczegoly/10267923" >6</a></span><span id="Ocena22" class="grade-box" style="background-color:#F0E68C; ">
            <a title="Kategoria: dz. 2<br>Data: 2018-11-25 (nd.)<br>Nauczyciel: Jaworski Maciej<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Jaworski Maciej<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/11124436" >3+</a></span><span id="Ocena23" class="grade-box" style="background-color:#F0E68C; ">
            <a title="Kategoria: RKO<br>Data: 2019-01-08 (wt.)<br>Nauczyciel: Jaworski Maciej<br>Licz do średniej: tak<br>Waga: 4<br>Dodał: Jaworski Maciej<br/>Obowiązek wyk. zadania: TAK<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/18868894" >5-</a></span><span id="Ocena24" class="grade-box" style="background-color:#F0E68C; ">
            <a title="Kategoria: pozycja boczna bezpieczna<br>Data: 2019-01-08 (wt.)<br>Nauczyciel: Jaworski Maciej<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Jaworski Maciej<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/18869031" >5-</a></span><span id="Ocena25" class="grade-box" style="background-color:#87CEFA; ">
            <a title="Kategoria: zeszyt<br>Data: 2019-01-22 (wt.)<br>Nauczyciel: Jaworski Maciej<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Jaworski Maciej<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/21805574" >5</a></span></td><td class="right">5.00</td><td class="center" > - </td><td class="center" ><span id="Ocena26" class="grade-box" style="background-color:#B0C4DE; ">
            <a title="Kategoria: śródroczna<br>Data: 2019-01-22 (wt.)<br>Nauczyciel: Jaworski Maciej<br>Licz do średniej: nie<br>Dodał: Jaworski Maciej<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/21809547" >5</a></span></td><td ><span id="Ocena27" class="grade-box" style="background-color:#F0E68C; ">
            <a title="Kategoria: pozycja boczna bezpieczna<br>Data: 2019-03-12 (wt.)<br>Nauczyciel: Jaworski Maciej<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Jaworski Maciej<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/27646025" >5-</a></span><span id="Ocena28" class="grade-box" style="background-color:#F0E68C; ">
            <a title="Kategoria: RKO<br>Data: 2019-03-12 (wt.)<br>Nauczyciel: Jaworski Maciej<br>Licz do średniej: tak<br>Waga: 4<br>Dodał: Jaworski Maciej<br/>Obowiązek wyk. zadania: TAK<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/27644642" >5-</a></span></td><td  class="right">4.75</td><td class="center"  > - </td><td  class="right"  >4.94</td><td class="center"  > - </td><td class="center"  > - </td></tr><tr class="line0" name="przedmioty_all" id="przedmioty_32793" style="display: none; visibility: hidden;">
                              <td colspan="14" ><table class="stretch"><thead><tr class=""><td >Ocena</td><td title="Komentarz do oceny">K</td><td>Kategoria</td><td class="center">Data</td><td>Nauczyciel</td><td>Licz do śr.</td><td>Waga</td><td >Poprawa oceny</td><td>Dodał</td></tr></thead><tbody><tr class="line1 bolded"><td colspan="9" class="center">Okres 1</td></tr><tr class="line1 detail-grades" style="background-color: #F0E68C;"><td class="center">6</td><td class="center">&nbsp;</td><td>dz.1</td><td class="center">2018-10-02</td><td >Jaworski Maciej</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Jaworski Maciej</td></tr><tr class="line1 detail-grades" style="background-color: #F0E68C;"><td class="center">6</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/10267923','ko',480,450)" >K</a></strong></td><td>dz 3</td><td class="center">2018-11-20</td><td >Jaworski Maciej</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Jaworski Maciej</td></tr><tr class="line1 detail-grades" style="background-color: #F0E68C;"><td class="center">3+</td><td class="center">&nbsp;</td><td>dz. 2</td><td class="center">2018-11-25</td><td >Jaworski Maciej</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Jaworski Maciej</td></tr><tr class="line1 detail-grades" style="background-color: #F0E68C;"><td class="center">5-</td><td class="center">&nbsp;</td><td>RKO</td><td class="center">2019-01-08</td><td >Jaworski Maciej</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >4</td><td class="center" >-</td><td >Jaworski Maciej</td></tr><tr class="line1 detail-grades" style="background-color: #F0E68C;"><td class="center">5-</td><td class="center">&nbsp;</td><td>pozycja boczna bezpieczna</td><td class="center">2019-01-08</td><td >Jaworski Maciej</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Jaworski Maciej</td></tr><tr class="line1 detail-grades" style="background-color: #B0C4DE;"><td class="center">5</td><td class="center">&nbsp;</td><td>śródroczna</td><td class="center">2019-01-22</td><td >Jaworski Maciej</td><td class="center"><img src="/images/nieaktywne.png"  border="0" /></td><td class="right" ></td><td class="center" >-</td><td >Jaworski Maciej</td></tr><tr class="line1 detail-grades" style="background-color: #87CEFA;"><td class="center">5</td><td class="center">&nbsp;</td><td>zeszyt</td><td class="center">2019-01-22</td><td >Jaworski Maciej</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Jaworski Maciej</td></tr><tr class="line1 bolded"><td colspan="9" class="center">Okres 2</td></tr><tr class="line1 detail-grades" style="background-color: #F0E68C;"><td class="center">5-</td><td class="center">&nbsp;</td><td>pozycja boczna bezpieczna</td><td class="center">2019-03-12</td><td >Jaworski Maciej</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Jaworski Maciej</td></tr><tr class="line1 detail-grades" style="background-color: #F0E68C;"><td class="center">5-</td><td class="center">&nbsp;</td><td>RKO</td><td class="center">2019-03-12</td><td >Jaworski Maciej</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >4</td><td class="center" >-</td><td >Jaworski Maciej</td></tr></tbody><tfoot><tr><td  class="center" colspan="14"></td></tr></tfoot></table></td></tr><tr class="line1">
                            <td class='center micro screen-only'><img src="/images/tree_colapsed.png" id="przedmioty_32777_node" onclick="showHide.ShowHide(32777);" /></td>
                            <td >Fizyka</td><td ><span id="Ocena29" class="grade-box" style="background-color:#FF8C00; ">
            <a title="Kategoria: 4_Praca na lekcji<br>Data: 2018-10-03 (śr.)<br>Nauczyciel: Gajtka Mirosław<br>Dodał: Gajtka Mirosław<br/><br>Komentarz: Elektrostatyka [%]" class="ocena" href="/przegladaj_oceny/szczegoly/2795254" >-</a></span><span id="Ocena30" class="grade-box" style="background-color:#FF0000; ">
            <a title="Kategoria: 1_Praca klasowa<br>Data: 2018-10-16 (wt.)<br>Nauczyciel: Gajtka Mirosław<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Gajtka Mirosław<br/><br>Komentarz: Elektrostatyka [56%]" class="ocena" href="/przegladaj_oceny/szczegoly/4999276" >3-</a></span><span id="Ocena31" class="grade-box" style="background-color:#ADFF2F; ">
            <a title="Kategoria: 4_Ćwiczenia<br>Data: 2018-10-17 (śr.)<br>Nauczyciel: Gajtka Mirosław<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Gajtka Mirosław<br/><br>Komentarz: roz 1/2/4/5 [84%]" class="ocena" href="/przegladaj_oceny/szczegoly/5066033" >4+</a></span><span id="Ocena32" class="grade-box" style="background-color:#FF1493; ">
            <a title="Kategoria: 2_Poprawa pracy klasowej<br>Data: 2018-11-07 (śr.)<br>Nauczyciel: Gajtka Mirosław<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Gajtka Mirosław<br/><br>Komentarz: Elektrostatyka [75%]" class="ocena" href="/przegladaj_oceny/szczegoly/8337522" >4-</a></span><span id="Ocena33" class="grade-box" style="background-color:#87CEFA; ">
            <a title="Kategoria: 3_Odpowiedź<br>Data: 2018-11-07 (śr.)<br>Nauczyciel: Gajtka Mirosław<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Gajtka Mirosław<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/8308680" >4+</a></span><span id="Ocena34" class="grade-box" style="background-color:#BDB76B; ">
            <a title="Kategoria: Aktywność<br>Data: 2018-11-14 (śr.)<br>Nauczyciel: Gajtka Mirosław<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Gajtka Mirosław<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/9263040" >5</a></span><span id="Ocena35" class="grade-box" style="background-color:#ADFF2F; ">
            <a title="Kategoria: 4_Ćwiczenia<br>Data: 2018-11-21 (śr.)<br>Nauczyciel: Gajtka Mirosław<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Gajtka Mirosław<br/><br>Komentarz: roz 8/10a/10b 86[%]" class="ocena" href="/przegladaj_oceny/szczegoly/10617474" >5-</a></span><span id="Ocena36" class="grade-box" style="background-color:#FF0000; ">
            <a title="Kategoria: 1_Praca klasowa<br>Data: 2018-11-28 (śr.)<br>Nauczyciel: Gajtka Mirosław<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Gajtka Mirosław<br/><br>Komentarz: Prąd elektryczny [75%]" class="ocena" href="/przegladaj_oceny/szczegoly/11716520" >4-</a></span><span id="Ocena37" class="grade-box" style="background-color:#F0E68C; ">
            <a title="Kategoria: Doświadczenia<br>Data: 2018-12-12 (śr.)<br>Nauczyciel: Gajtka Mirosław<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Gajtka Mirosław<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/14815012" >5</a></span><span id="Ocena38" class="grade-box" style="background-color:#ADFF2F; ">
            <a title="Kategoria: 4_Ćwiczenia<br>Data: 2019-01-09 (śr.)<br>Nauczyciel: Gajtka Mirosław<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Gajtka Mirosław<br/><br>Komentarz: Magnetyzm [79%]" class="ocena" href="/przegladaj_oceny/szczegoly/19414251" >4-</a></span><span id="Ocena39" class="grade-box" style="background-color:#F0F8FF; ">
            <a title="Ocena: nieprzygotowany<br>Kategoria: Nieprzygotowanie<br>Data: 2019-01-09 (śr.)<br>Nauczyciel: Gajtka Mirosław<br>Dodał: Gajtka Mirosław<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/19249744" >np</a></span><span id="Ocena40" class="grade-box" style="background-color:#F0F8FF; ">
            <a title="Ocena: nieprzygotowany<br>Kategoria: Nieprzygotowanie<br>Data: 2019-01-16 (śr.)<br>Nauczyciel: Gajtka Mirosław<br>Dodał: Gajtka Mirosław<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/20914582" >np</a></span><span id="Ocena41" class="grade-box" style="background-color:#FF0000; ">
            <a title="Kategoria: 1_Praca klasowa<br>Data: 2019-01-23 (śr.)<br>Nauczyciel: Gajtka Mirosław<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Gajtka Mirosław<br/><br>Komentarz: Magnetyzm [77%]" class="ocena" href="/przegladaj_oceny/szczegoly/21957345" >4-</a></span></td><td class="right">3.89</td><td class="center" > - </td><td class="center" ><span id="Ocena42" class="grade-box" style="background-color:#B0C4DE; ">
            <a title="Kategoria: śródroczna<br>Data: 2019-01-23 (śr.)<br>Nauczyciel: Gajtka Mirosław<br>Licz do średniej: nie<br>Dodał: Gajtka Mirosław<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/21971643" >4</a></span></td><td ><span id="Ocena43" class="grade-box" style="background-color:#66CDAA; ">
            <a title="Kategoria: Praca w grupach<br>Data: 2019-03-12 (wt.)<br>Nauczyciel: Gajtka Mirosław<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Gajtka Mirosław<br/><br>Komentarz: Fala elektromagnetyczna [66%]" class="ocena" href="/przegladaj_oceny/szczegoly/27641751" >3</a></span><span id="Ocena44" class="grade-box" style="background-color:#FF8C00; ">
            <a title="Kategoria: 4_Praca na lekcji<br>Data: 2019-03-13 (śr.)<br>Nauczyciel: Gajtka Mirosław<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Gajtka Mirosław<br/><br>Komentarz: drgania i fale [86%]" class="ocena" href="/przegladaj_oceny/szczegoly/27927118" >5-</a></span><span id="Ocena45" class="grade-box" style="background-color:#FF0000; ">
            <a title="Kategoria: 1_Praca klasowa<br>Data: 2019-03-19 (wt.)<br>Nauczyciel: Gajtka Mirosław<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Gajtka Mirosław<br/><br>Komentarz: Drgania i fale [78%]" class="ocena" href="/przegladaj_oceny/szczegoly/29018953" >4-</a></span><span id="Ocena46" class="grade-box" style="background-color:#FF8C00; ">
            <a title="Kategoria: 4_Praca na lekcji<br>Data: 2019-04-03 (śr.)<br>Nauczyciel: Gajtka Mirosław<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Gajtka Mirosław<br/><br>Komentarz: Testy [83%]" class="ocena" href="/przegladaj_oceny/szczegoly/31527947" >4</a></span><span id="Ocena47" class="grade-box" style="background-color:#FF8C00; ">
            <a title="Kategoria: 4_Praca na lekcji<br>Data: 2019-04-03 (śr.)<br>Nauczyciel: Gajtka Mirosław<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Gajtka Mirosław<br/><br>Komentarz: Testy [90%]" class="ocena" href="/przegladaj_oceny/szczegoly/31531692" >5</a></span><span id="Ocena48" class="grade-box" style="background-color:#FF8C00; ">
            <a title="Kategoria: 4_Praca na lekcji<br>Data: 2019-04-03 (śr.)<br>Nauczyciel: Gajtka Mirosław<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Gajtka Mirosław<br/><br>Komentarz: Testy [94%]" class="ocena" href="/przegladaj_oceny/szczegoly/31530039" >5+</a></span></td><td  class="right">4.19</td><td class="center"  > - </td><td  class="right"  >3.98</td><td class="center"  > - </td><td class="center"  > - </td></tr><tr class="line1" name="przedmioty_all" id="przedmioty_32777" style="display: none; visibility: hidden;">
                              <td colspan="14" ><table class="stretch"><thead><tr class=""><td >Ocena</td><td title="Komentarz do oceny">K</td><td>Kategoria</td><td class="center">Data</td><td>Nauczyciel</td><td>Licz do śr.</td><td>Waga</td><td >Poprawa oceny</td><td>Dodał</td></tr></thead><tbody><tr class="line1 bolded"><td colspan="9" class="center">Okres 1</td></tr><tr class="line1 detail-grades" style="background-color: #FF8C00;"><td class="center">-</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/2795254','ko',480,450)" >K</a></strong></td><td>4_Praca na lekcji</td><td class="center">2018-10-03</td><td >Gajtka Mirosław</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Gajtka Mirosław</td></tr><tr class="line1 detail-grades" style="background-color: #FF0000;"><td class="center">3-</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/4999276','ko',480,450)" >K</a></strong></td><td>1_Praca klasowa</td><td class="center">2018-10-16</td><td >Gajtka Mirosław</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Gajtka Mirosław</td></tr><tr class="line1 detail-grades" style="background-color: #ADFF2F;"><td class="center">4+</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/5066033','ko',480,450)" >K</a></strong></td><td>4_Ćwiczenia</td><td class="center">2018-10-17</td><td >Gajtka Mirosław</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Gajtka Mirosław</td></tr><tr class="line1 detail-grades" style="background-color: #FF1493;"><td class="center">4-</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/8337522','ko',480,450)" >K</a></strong></td><td>2_Poprawa pracy klasowej</td><td class="center">2018-11-07</td><td >Gajtka Mirosław</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Gajtka Mirosław</td></tr><tr class="line1 detail-grades" style="background-color: #87CEFA;"><td class="center">4+</td><td class="center">&nbsp;</td><td>3_Odpowiedź</td><td class="center">2018-11-07</td><td >Gajtka Mirosław</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Gajtka Mirosław</td></tr><tr class="line1 detail-grades" style="background-color: #BDB76B;"><td class="center">5</td><td class="center">&nbsp;</td><td>Aktywność</td><td class="center">2018-11-14</td><td >Gajtka Mirosław</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Gajtka Mirosław</td></tr><tr class="line1 detail-grades" style="background-color: #ADFF2F;"><td class="center">5-</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/10617474','ko',480,450)" >K</a></strong></td><td>4_Ćwiczenia</td><td class="center">2018-11-21</td><td >Gajtka Mirosław</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Gajtka Mirosław</td></tr><tr class="line1 detail-grades" style="background-color: #FF0000;"><td class="center">4-</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/11716520','ko',480,450)" >K</a></strong></td><td>1_Praca klasowa</td><td class="center">2018-11-28</td><td >Gajtka Mirosław</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Gajtka Mirosław</td></tr><tr class="line1 detail-grades" style="background-color: #F0E68C;"><td class="center">5</td><td class="center">&nbsp;</td><td>Doświadczenia</td><td class="center">2018-12-12</td><td >Gajtka Mirosław</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Gajtka Mirosław</td></tr><tr class="line1 detail-grades" style="background-color: #ADFF2F;"><td class="center">4-</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/19414251','ko',480,450)" >K</a></strong></td><td>4_Ćwiczenia</td><td class="center">2019-01-09</td><td >Gajtka Mirosław</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Gajtka Mirosław</td></tr><tr class="line1 detail-grades" style="background-color: #F0F8FF;"><td class="center">np</td><td class="center">&nbsp;</td><td>Nieprzygotowanie</td><td class="center">2019-01-09</td><td >Gajtka Mirosław</td><td class="center"><img src="/images/nieaktywne.png"  border="0" /></td><td class="right" ></td><td class="center" >-</td><td >Gajtka Mirosław</td></tr><tr class="line1 detail-grades" style="background-color: #F0F8FF;"><td class="center">np</td><td class="center">&nbsp;</td><td>Nieprzygotowanie</td><td class="center">2019-01-16</td><td >Gajtka Mirosław</td><td class="center"><img src="/images/nieaktywne.png"  border="0" /></td><td class="right" ></td><td class="center" >-</td><td >Gajtka Mirosław</td></tr><tr class="line1 detail-grades" style="background-color: #B0C4DE;"><td class="center">4</td><td class="center">&nbsp;</td><td>śródroczna</td><td class="center">2019-01-23</td><td >Gajtka Mirosław</td><td class="center"><img src="/images/nieaktywne.png"  border="0" /></td><td class="right" ></td><td class="center" >-</td><td >Gajtka Mirosław</td></tr><tr class="line1 detail-grades" style="background-color: #FF0000;"><td class="center">4-</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/21957345','ko',480,450)" >K</a></strong></td><td>1_Praca klasowa</td><td class="center">2019-01-23</td><td >Gajtka Mirosław</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Gajtka Mirosław</td></tr><tr class="line1 bolded"><td colspan="9" class="center">Okres 2</td></tr><tr class="line1 detail-grades" style="background-color: #66CDAA;"><td class="center">3</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/27641751','ko',480,450)" >K</a></strong></td><td>Praca w grupach</td><td class="center">2019-03-12</td><td >Gajtka Mirosław</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Gajtka Mirosław</td></tr><tr class="line1 detail-grades" style="background-color: #FF8C00;"><td class="center">5-</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/27927118','ko',480,450)" >K</a></strong></td><td>4_Praca na lekcji</td><td class="center">2019-03-13</td><td >Gajtka Mirosław</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Gajtka Mirosław</td></tr><tr class="line1 detail-grades" style="background-color: #FF0000;"><td class="center">4-</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/29018953','ko',480,450)" >K</a></strong></td><td>1_Praca klasowa</td><td class="center">2019-03-19</td><td >Gajtka Mirosław</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Gajtka Mirosław</td></tr><tr class="line1 detail-grades" style="background-color: #FF8C00;"><td class="center">4</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/31527947','ko',480,450)" >K</a></strong></td><td>4_Praca na lekcji</td><td class="center">2019-04-03</td><td >Gajtka Mirosław</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Gajtka Mirosław</td></tr><tr class="line1 detail-grades" style="background-color: #FF8C00;"><td class="center">5</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/31531692','ko',480,450)" >K</a></strong></td><td>4_Praca na lekcji</td><td class="center">2019-04-03</td><td >Gajtka Mirosław</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Gajtka Mirosław</td></tr><tr class="line1 detail-grades" style="background-color: #FF8C00;"><td class="center">5+</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/31530039','ko',480,450)" >K</a></strong></td><td>4_Praca na lekcji</td><td class="center">2019-04-03</td><td >Gajtka Mirosław</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Gajtka Mirosław</td></tr></tbody><tfoot><tr><td  class="center" colspan="14"></td></tr></tfoot></table></td></tr><tr class="line0">
                            <td class='center micro screen-only'><img src="/images/tree_colapsed.png" id="przedmioty_32778_node" onclick="showHide.ShowHide(32778);" /></td>
                            <td >Geografia</td><td ><span id="Ocena49" class="grade-box" style="background-color:#3333FF; ">
            <a title="Kategoria: mały sprawdzian<br>Data: 2018-09-21 (pt.)<br>Nauczyciel: Niemczyk Katarzyna<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Niemczyk Katarzyna<br/><br>Komentarz: mapa polityczna Europy" class="ocena" href="/przegladaj_oceny/szczegoly/1107494" >4</a></span><span id="Ocena50" class="grade-box" style="background-color:#DC143C; ">
            <a title="Kategoria: praca klasowa<br>Data: 2018-11-20 (wt.)<br>Nauczyciel: Niemczyk Katarzyna<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Niemczyk Katarzyna<br/>Obowiązek wyk. zadania: TAK<br/><br>Komentarz: E uropa" class="ocena" href="/przegladaj_oceny/szczegoly/10314671" >3</a></span><span id="Ocena51" class="grade-box" style="background-color:#3333FF; ">
            <a title="Kategoria: mały sprawdzian<br>Data: 2018-12-11 (wt.)<br>Nauczyciel: Niemczyk Katarzyna<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Niemczyk Katarzyna<br/><br>Komentarz: sąsiedzi Polski" class="ocena" href="/przegladaj_oceny/szczegoly/14353679" >3+</a></span><span id="Ocena52" class="grade-box" style="background-color:#32CD32; ">
            <a title="Kategoria: poprawa sprawdzianu<br>Data: 2019-01-02 (śr.)<br>Nauczyciel: Niemczyk Katarzyna<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Niemczyk Katarzyna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/17357698" >3+</a></span></td><td class="right">3.45</td><td class="center" > - </td><td class="center" ><span id="Ocena53" class="grade-box" style="background-color:#B0C4DE; ">
            <a title="Kategoria: śródroczna<br>Data: 2019-01-24 (czw.)<br>Nauczyciel: Niemczyk Katarzyna<br>Licz do średniej: nie<br>Dodał: Niemczyk Katarzyna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/22128766" >3+</a></span></td><td ><span id="Ocena54" class="grade-box" style="background-color:#DC143C; ">
            <a title="Kategoria: praca klasowa<br>Data: 2019-01-29 (wt.)<br>Nauczyciel: Niemczyk Katarzyna<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Niemczyk Katarzyna<br/>Obowiązek wyk. zadania: TAK<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/22388503" >3+</a></span><span id="Ocena55" class="grade-box" style="background-color:#DC143C; ">
            <a title="Kategoria: praca klasowa<br>Data: 2019-02-26 (wt.)<br>Nauczyciel: Niemczyk Katarzyna<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Niemczyk Katarzyna<br/>Obowiązek wyk. zadania: TAK<br/><br>Komentarz: surowce,geologia,klimat" class="ocena" href="/przegladaj_oceny/szczegoly/25391945" >4</a></span><span id="Ocena56" class="grade-box" style="background-color:#32CD32; ">
            <a title="Kategoria: poprawa sprawdzianu<br>Data: 2019-02-26 (wt.)<br>Nauczyciel: Niemczyk Katarzyna<br>Dodał: Niemczyk Katarzyna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/25373476" >-</a></span><span id="Ocena57" class="grade-box" style="background-color:#DC143C; ">
            <a title="Kategoria: praca klasowa<br>Data: 2019-04-12 (pt.)<br>Nauczyciel: Niemczyk Katarzyna<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Niemczyk Katarzyna<br/>Obowiązek wyk. zadania: TAK<br/><br>Komentarz: ludność" class="ocena" href="/przegladaj_oceny/szczegoly/32407265" >3</a></span></td><td  class="right">3.50</td><td class="center"  > - </td><td  class="right"  >3.47</td><td class="center"  ><span id="Ocena58" class="grade-box" style="background-color:#F0FFFF; ">
            <a title="Kategoria: przewidywana roczna<br>Data: 2019-04-28 (nd.)<br>Nauczyciel: Niemczyk Katarzyna<br>Licz do średniej: nie<br>Dodał: Niemczyk Katarzyna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/32743938" >3+</a></span></td><td class="center"  > - </td></tr><tr class="line0" name="przedmioty_all" id="przedmioty_32778" style="display: none; visibility: hidden;">
                              <td colspan="14" ><table class="stretch"><thead><tr class=""><td >Ocena</td><td title="Komentarz do oceny">K</td><td>Kategoria</td><td class="center">Data</td><td>Nauczyciel</td><td>Licz do śr.</td><td>Waga</td><td >Poprawa oceny</td><td>Dodał</td></tr></thead><tbody><tr class="line1 bolded"><td colspan="9" class="center">Okres 1</td></tr><tr class="line1 detail-grades" style="background-color: #3333FF;"><td class="center">4</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/1107494','ko',480,450)" >K</a></strong></td><td>mały sprawdzian</td><td class="center">2018-09-21</td><td >Niemczyk Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Niemczyk Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #DC143C;"><td class="center">3</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/10314671','ko',480,450)" >K</a></strong></td><td>praca klasowa</td><td class="center">2018-11-20</td><td >Niemczyk Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Niemczyk Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #3333FF;"><td class="center">3+</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/14353679','ko',480,450)" >K</a></strong></td><td>mały sprawdzian</td><td class="center">2018-12-11</td><td >Niemczyk Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Niemczyk Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #32CD32;"><td class="center">3+</td><td class="center">&nbsp;</td><td>poprawa sprawdzianu</td><td class="center">2019-01-02</td><td >Niemczyk Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Niemczyk Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #B0C4DE;"><td class="center">3+</td><td class="center">&nbsp;</td><td>śródroczna</td><td class="center">2019-01-24</td><td >Niemczyk Katarzyna</td><td class="center"><img src="/images/nieaktywne.png"  border="0" /></td><td class="right" ></td><td class="center" >-</td><td >Niemczyk Katarzyna</td></tr><tr class="line1 bolded"><td colspan="9" class="center">Okres 2</td></tr><tr class="line1 detail-grades" style="background-color: #DC143C;"><td class="center">3+</td><td class="center">&nbsp;</td><td>praca klasowa</td><td class="center">2019-01-29</td><td >Niemczyk Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Niemczyk Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #DC143C;"><td class="center">4</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/25391945','ko',480,450)" >K</a></strong></td><td>praca klasowa</td><td class="center">2019-02-26</td><td >Niemczyk Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Niemczyk Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #32CD32;"><td class="center">-</td><td class="center">&nbsp;</td><td>poprawa sprawdzianu</td><td class="center">2019-02-26</td><td >Niemczyk Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Niemczyk Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #DC143C;"><td class="center">3</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/32407265','ko',480,450)" >K</a></strong></td><td>praca klasowa</td><td class="center">2019-04-12</td><td >Niemczyk Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Niemczyk Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #F0FFFF;"><td class="center">3+</td><td class="center">&nbsp;</td><td>przewidywana roczna</td><td class="center">2019-04-28</td><td >Niemczyk Katarzyna</td><td class="center"><img src="/images/nieaktywne.png"  border="0" /></td><td class="right" ></td><td class="center" >-</td><td >Niemczyk Katarzyna</td></tr></tbody><tfoot><tr><td  class="center" colspan="14"></td></tr></tfoot></table></td></tr><tr class="line1">
                            <td class='center micro screen-only'><img src="/images/tree_colapsed.png" id="przedmioty_32780_node" onclick="showHide.ShowHide(32780);" /></td>
                            <td >Historia</td><td ><span id="Ocena59" class="grade-box" style="background-color:#87CEFA; ">
            <a title="Kategoria: Oświecenie<br>Data: 2018-09-25 (wt.)<br>Nauczyciel: Zakarczemna Anna<br>Dodał: Zakarczemna Anna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/1666175" >-</a></span><span id="Ocena60" class="grade-box" style="background-color:#87CEFA; ">
            <a title="Kategoria: Oświecenie<br>Data: 2018-10-04 (czw.)<br>Nauczyciel: Zakarczemna Anna<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Zakarczemna Anna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/3096798" >5</a></span><span>[<span id="Ocena61" class="grade-box" style="background-color:#FFB6C1; ">
            <a title="Kategoria: Napoleon<br>Data: 2018-10-23 (wt.)<br>Nauczyciel: Zakarczemna Anna<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Zakarczemna Anna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/6005597" >3</a></span><span id="Ocena62" class="grade-box" style="background-color:#FFB6C1; ">
            <a title="Kategoria: Napoleon<br>Data: 2018-11-14 (śr.)<br>Nauczyciel: Zakarczemna Anna<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Zakarczemna Anna<br/>&lt;br /&gt;Poprawa oceny: 3 (Napoleon)" class="ocena" href="/przegladaj_oceny/szczegoly/9240874" >4-</a></span>]</span><span id="Ocena63" class="grade-box" style="background-color:#FFB6C1; ">
            <a title="Kategoria: odpowiedź ustna<br>Data: 2018-11-23 (pt.)<br>Nauczyciel: Zakarczemna Anna<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Zakarczemna Anna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/10941423" >5</a></span><span id="Ocena64" class="grade-box" style="background-color:#FFD700; ">
            <a title="Kategoria: Rozbiory <br>Data: 2018-11-27 (wt.)<br>Nauczyciel: Zakarczemna Anna<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Zakarczemna Anna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/11525885" >4</a></span><span id="Ocena65" class="grade-box" style="background-color:#DAA520; ">
            <a title="Kategoria: 1 połowa XIX w.<br>Data: 2019-01-10 (czw.)<br>Nauczyciel: Zakarczemna Anna<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Zakarczemna Anna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/19560195" >5-</a></span><span id="Ocena66" class="grade-box" style="background-color:#F0FFFF; ">
            <a title="Kategoria: nieprzygotowanie<br>Data: 2019-01-21 (pon.)<br>Nauczyciel: Zakarczemna Anna<br>Dodał: Zakarczemna Anna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/21637717" >-</a></span></td><td class="right">4.36</td><td class="center" > - </td><td class="center" ><span id="Ocena67" class="grade-box" style="background-color:#B0C4DE; ">
            <a title="Kategoria: śródroczna<br>Data: 2019-01-25 (pt.)<br>Nauczyciel: Zakarczemna Anna<br>Licz do średniej: nie<br>Dodał: Zakarczemna Anna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/22174367" >4</a></span></td><td ><span id="Ocena68" class="grade-box" style="background-color:#ADFF2F; ">
            <a title="Kategoria: 2 poł. XIX wieku<br>Data: 2019-02-25 (pon.)<br>Nauczyciel: Zakarczemna Anna<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Zakarczemna Anna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/25142222" >5-</a></span></td><td  class="right">4.75</td><td class="center"  > - </td><td  class="right"  >4.43</td><td class="center"  > - </td><td class="center"  > - </td></tr><tr class="line1" name="przedmioty_all" id="przedmioty_32780" style="display: none; visibility: hidden;">
                              <td colspan="14" ><table class="stretch"><thead><tr class=""><td >Ocena</td><td title="Komentarz do oceny">K</td><td>Kategoria</td><td class="center">Data</td><td>Nauczyciel</td><td>Licz do śr.</td><td>Waga</td><td >Poprawa oceny</td><td>Dodał</td></tr></thead><tbody><tr class="line1 bolded"><td colspan="9" class="center">Okres 1</td></tr><tr class="line1 detail-grades" style="background-color: #87CEFA;"><td class="center">-</td><td class="center">&nbsp;</td><td>Oświecenie</td><td class="center">2018-09-25</td><td >Zakarczemna Anna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Zakarczemna Anna</td></tr><tr class="line1 detail-grades" style="background-color: #87CEFA;"><td class="center">5</td><td class="center">&nbsp;</td><td>Oświecenie</td><td class="center">2018-10-04</td><td >Zakarczemna Anna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Zakarczemna Anna</td></tr><tr class="line1 detail-grades" style="background-color: #FFB6C1;"><td class="center">3</td><td class="center">&nbsp;</td><td>Napoleon</td><td class="center">2018-10-23</td><td >Zakarczemna Anna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Zakarczemna Anna</td></tr><tr class="line1 detail-grades" style="background-color: #FFB6C1;"><td class="center">4-</td><td class="center">&nbsp;</td><td>Napoleon</td><td class="center">2018-11-14</td><td >Zakarczemna Anna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >3 (Napoleon)</td><td >Zakarczemna Anna</td></tr><tr class="line1 detail-grades" style="background-color: #FFB6C1;"><td class="center">5</td><td class="center">&nbsp;</td><td>odpowiedź ustna</td><td class="center">2018-11-23</td><td >Zakarczemna Anna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Zakarczemna Anna</td></tr><tr class="line1 detail-grades" style="background-color: #FFD700;"><td class="center">4</td><td class="center">&nbsp;</td><td>Rozbiory </td><td class="center">2018-11-27</td><td >Zakarczemna Anna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Zakarczemna Anna</td></tr><tr class="line1 detail-grades" style="background-color: #DAA520;"><td class="center">5-</td><td class="center">&nbsp;</td><td>1 połowa XIX w.</td><td class="center">2019-01-10</td><td >Zakarczemna Anna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Zakarczemna Anna</td></tr><tr class="line1 detail-grades" style="background-color: #F0FFFF;"><td class="center">-</td><td class="center">&nbsp;</td><td>nieprzygotowanie</td><td class="center">2019-01-21</td><td >Zakarczemna Anna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Zakarczemna Anna</td></tr><tr class="line1 detail-grades" style="background-color: #B0C4DE;"><td class="center">4</td><td class="center">&nbsp;</td><td>śródroczna</td><td class="center">2019-01-25</td><td >Zakarczemna Anna</td><td class="center"><img src="/images/nieaktywne.png"  border="0" /></td><td class="right" ></td><td class="center" >-</td><td >Zakarczemna Anna</td></tr><tr class="line1 bolded"><td colspan="9" class="center">Okres 2</td></tr><tr class="line1 detail-grades" style="background-color: #ADFF2F;"><td class="center">5-</td><td class="center">&nbsp;</td><td>2 poł. XIX wieku</td><td class="center">2019-02-25</td><td >Zakarczemna Anna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Zakarczemna Anna</td></tr></tbody><tfoot><tr><td  class="center" colspan="14"></td></tr></tfoot></table></td></tr><tr class="line0">
                            <td class='center micro screen-only'><img src="/images/tree_colapsed.png" id="przedmioty_32781_node" onclick="showHide.ShowHide(32781);" /></td>
                            <td >Informatyka</td><td ><span id="Ocena69" class="grade-box" style="background-color:#FF0000; ">
            <a title="Kategoria: sprawdzian<br>Data: 2018-10-18 (czw.)<br>Nauczyciel: Gawroński Jacek<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Gawroński Jacek<br/><br>Komentarz: Sprzęt komputerowy, informatyczne jednostki miary, prawo autorskie" class="ocena" href="/przegladaj_oceny/szczegoly/5385298" >5</a></span><span id="Ocena70" class="grade-box" style="background-color:#BA55D3; ">
            <a title="Kategoria: konkursy<br>Data: 2018-11-22 (czw.)<br>Nauczyciel: Gawroński Jacek<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Gawroński Jacek<br/><br>Komentarz: Kuratoryjny Konkurs Informatyczny - etap szkolny" class="ocena" href="/przegladaj_oceny/szczegoly/10859339" >6</a></span><span id="Ocena71" class="grade-box" style="background-color:#FF0000; ">
            <a title="Kategoria: sprawdzian<br>Data: 2018-11-27 (wt.)<br>Nauczyciel: Gawroński Jacek<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Gawroński Jacek<br/><br>Komentarz: Arkusz kalkulacyjny" class="ocena" href="/przegladaj_oceny/szczegoly/11660934" >5-</a></span><span id="Ocena72" class="grade-box" style="background-color:#BA55D3; ">
            <a title="Kategoria: konkursy<br>Data: 2018-11-28 (śr.)<br>Nauczyciel: Gawroński Jacek<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Gawroński Jacek<br/><br>Komentarz: Ogólnopolski Konkurs Informatyczny BÓBR" class="ocena" href="/przegladaj_oceny/szczegoly/11857303" >6</a></span><span id="Ocena73" class="grade-box" style="background-color:#FF0000; ">
            <a title="Kategoria: sprawdzian<br>Data: 2019-01-17 (czw.)<br>Nauczyciel: Gawroński Jacek<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Gawroński Jacek<br/><br>Komentarz: Baza danych" class="ocena" href="/przegladaj_oceny/szczegoly/21257073" >5-</a></span></td><td class="right">5.19</td><td class="center" ><span id="Ocena74" class="grade-box" style="background-color:#F0F8FF; ">
            <a title="Kategoria: przewidywana śródroczna<br>Data: 2018-11-22 (czw.)<br>Nauczyciel: Gawroński Jacek<br>Licz do średniej: nie<br>Dodał: Gawroński Jacek<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/10859369" >6</a></span></td><td class="center" ><span id="Ocena75" class="grade-box" style="background-color:#B0C4DE; ">
            <a title="Kategoria: śródroczna<br>Data: 2019-01-24 (czw.)<br>Nauczyciel: Gawroński Jacek<br>Licz do średniej: nie<br>Dodał: Gawroński Jacek<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/22130380" >6</a></span></td><td ><span id="Ocena76" class="grade-box" style="background-color:#BA55D3; ">
            <a title="Kategoria: konkursy<br>Data: 2019-01-31 (czw.)<br>Nauczyciel: Gawroński Jacek<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Gawroński Jacek<br/><br>Komentarz: Kuratoryjny Konkurs Informatyczny - finał wojewódzki" class="ocena" href="/przegladaj_oceny/szczegoly/22706852" >6</a></span><span id="Ocena77" class="grade-box" style="background-color:#87CEFA; ">
            <a title="Kategoria: projekt<br>Data: 2019-03-14 (czw.)<br>Nauczyciel: Gawroński Jacek<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Gawroński Jacek<br/><br>Komentarz: Grafika komputerowa" class="ocena" href="/przegladaj_oceny/szczegoly/28190994" >5</a></span><span id="Ocena78" class="grade-box" style="background-color:#87CEFA; ">
            <a title="Kategoria: projekt<br>Data: 2019-04-25 (czw.)<br>Nauczyciel: Gawroński Jacek<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Gawroński Jacek<br/><br>Komentarz: Broszura DTP" class="ocena" href="/przegladaj_oceny/szczegoly/32667485" >5</a></span></td><td  class="right">5.33</td><td class="center"  > - </td><td  class="right"  >5.24</td><td class="center"  > - </td><td class="center"  > - </td></tr><tr class="line0" name="przedmioty_all" id="przedmioty_32781" style="display: none; visibility: hidden;">
                              <td colspan="14" ><table class="stretch"><thead><tr class=""><td >Ocena</td><td title="Komentarz do oceny">K</td><td>Kategoria</td><td class="center">Data</td><td>Nauczyciel</td><td>Licz do śr.</td><td>Waga</td><td >Poprawa oceny</td><td>Dodał</td></tr></thead><tbody><tr class="line1 bolded"><td colspan="9" class="center">Okres 1</td></tr><tr class="line1 detail-grades" style="background-color: #FF0000;"><td class="center">5</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/5385298','ko',480,450)" >K</a></strong></td><td>sprawdzian</td><td class="center">2018-10-18</td><td >Gawroński Jacek</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Gawroński Jacek</td></tr><tr class="line1 detail-grades" style="background-color: #F0F8FF;"><td class="center">6</td><td class="center">&nbsp;</td><td>przewidywana śródroczna</td><td class="center">2018-11-22</td><td >Gawroński Jacek</td><td class="center"><img src="/images/nieaktywne.png"  border="0" /></td><td class="right" ></td><td class="center" >-</td><td >Gawroński Jacek</td></tr><tr class="line1 detail-grades" style="background-color: #BA55D3;"><td class="center">6</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/10859339','ko',480,450)" >K</a></strong></td><td>konkursy</td><td class="center">2018-11-22</td><td >Gawroński Jacek</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Gawroński Jacek</td></tr><tr class="line1 detail-grades" style="background-color: #FF0000;"><td class="center">5-</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/11660934','ko',480,450)" >K</a></strong></td><td>sprawdzian</td><td class="center">2018-11-27</td><td >Gawroński Jacek</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Gawroński Jacek</td></tr><tr class="line1 detail-grades" style="background-color: #BA55D3;"><td class="center">6</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/11857303','ko',480,450)" >K</a></strong></td><td>konkursy</td><td class="center">2018-11-28</td><td >Gawroński Jacek</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Gawroński Jacek</td></tr><tr class="line1 detail-grades" style="background-color: #FF0000;"><td class="center">5-</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/21257073','ko',480,450)" >K</a></strong></td><td>sprawdzian</td><td class="center">2019-01-17</td><td >Gawroński Jacek</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Gawroński Jacek</td></tr><tr class="line1 detail-grades" style="background-color: #B0C4DE;"><td class="center">6</td><td class="center">&nbsp;</td><td>śródroczna</td><td class="center">2019-01-24</td><td >Gawroński Jacek</td><td class="center"><img src="/images/nieaktywne.png"  border="0" /></td><td class="right" ></td><td class="center" >-</td><td >Gawroński Jacek</td></tr><tr class="line1 bolded"><td colspan="9" class="center">Okres 2</td></tr><tr class="line1 detail-grades" style="background-color: #BA55D3;"><td class="center">6</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/22706852','ko',480,450)" >K</a></strong></td><td>konkursy</td><td class="center">2019-01-31</td><td >Gawroński Jacek</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Gawroński Jacek</td></tr><tr class="line1 detail-grades" style="background-color: #87CEFA;"><td class="center">5</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/28190994','ko',480,450)" >K</a></strong></td><td>projekt</td><td class="center">2019-03-14</td><td >Gawroński Jacek</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Gawroński Jacek</td></tr><tr class="line1 detail-grades" style="background-color: #87CEFA;"><td class="center">5</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/32667485','ko',480,450)" >K</a></strong></td><td>projekt</td><td class="center">2019-04-25</td><td >Gawroński Jacek</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Gawroński Jacek</td></tr></tbody><tfoot><tr><td  class="center" colspan="14"></td></tr></tfoot></table></td></tr><tr class="line1">
                            <td class='center micro screen-only'><img src="/images/tree_colapsed.png" id="przedmioty_32782_node" onclick="showHide.ShowHide(32782);" /></td>
                            <td >Język angielski</td><td ><span id="Ocena79" class="grade-box" style="background-color:#E6E6FA; ">
            <a title="Kategoria: praca na lekcji(grupowa/indywidualna)<br>Data: 2018-09-06 (czw.)<br>Nauczyciel: Monińska Julita<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Monińska Julita<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/76749" >5</a></span><span id="Ocena80" class="grade-box" style="background-color:#87CEFA; ">
            <a title="Kategoria: kartkówka <br>Data: 2018-09-18 (wt.)<br>Nauczyciel: Monińska Julita<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Monińska Julita<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/805068" >4+</a></span><span id="Ocena81" class="grade-box" style="background-color:#87CEFA; ">
            <a title="Kategoria: kartkówka <br>Data: 2018-09-27 (czw.)<br>Nauczyciel: Monińska Julita<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Monińska Julita<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/1939322" >5</a></span><span id="Ocena82" class="grade-box" style="background-color:#7B68EE; ">
            <a title="Kategoria: praca domowa<br>Data: 2018-10-07 (nd.)<br>Nauczyciel: Monińska Julita<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Monińska Julita<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/3411235" >5</a></span><span id="Ocena83" class="grade-box" style="background-color:#FFA07A; ">
            <a title="Kategoria: Speech<br>Data: 2018-10-09 (wt.)<br>Nauczyciel: Monińska Julita<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Monińska Julita<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/3898768" >6</a></span><span id="Ocena84" class="grade-box" style="background-color:#7B68EE; ">
            <a title="Kategoria: praca domowa<br>Data: 2018-10-11 (czw.)<br>Nauczyciel: Monińska Julita<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Monińska Julita<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/4338936" >4</a></span><span id="Ocena85" class="grade-box" style="background-color:#ADFF2F; ">
            <a title="Kategoria: aktywność<br>Data: 2018-10-11 (czw.)<br>Nauczyciel: Monińska Julita<br>Dodał: Monińska Julita<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/4332686" >+</a></span><span id="Ocena86" class="grade-box" style="background-color:#66CDAA; ">
            <a title="Kategoria: prace pisemne<br>Data: 2018-10-28 (nd.)<br>Nauczyciel: Monińska Julita<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Monińska Julita<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/6938356" >6-</a></span><span id="Ocena87" class="grade-box" style="background-color:#FF1493; ">
            <a title="Kategoria: sprawdzian<br>Data: 2018-10-30 (wt.)<br>Nauczyciel: Monińska Julita<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Monińska Julita<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/7769501" >4-</a></span><span id="Ocena88" class="grade-box" style="background-color:#7CFC00; ">
            <a title="Kategoria: prezentacje<br>Data: 2018-10-31 (śr.)<br>Nauczyciel: Monińska Julita<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Monińska Julita<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/7770628" >6</a></span><span id="Ocena89" class="grade-box" style="background-color:#87CEFA; ">
            <a title="Kategoria: kartkówka <br>Data: 2018-11-15 (czw.)<br>Nauczyciel: Monińska Julita<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Monińska Julita<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/9681535" >5+</a></span><span id="Ocena90" class="grade-box" style="background-color:#F0E68C; ">
            <a title="Kategoria: sluchanie<br>Data: 2018-11-23 (pt.)<br>Nauczyciel: Monińska Julita<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Monińska Julita<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/11060117" >6</a></span><span id="Ocena91" class="grade-box" style="background-color:#F0E68C; ">
            <a title="Kategoria: aktywność<br>Data: 2018-12-10 (pon.)<br>Nauczyciel: Monińska Julita<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Monińska Julita<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/14111120" >6</a></span><span id="Ocena92" class="grade-box" style="background-color:#FFD700; ">
            <a title="Kategoria: Pisanie-egzamin<br>Data: 2019-01-06 (nd.)<br>Nauczyciel: Monińska Julita<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Monińska Julita<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/18448639" >2</a></span><span id="Ocena93" class="grade-box" style="background-color:#87CEFA; ">
            <a title="Kategoria: kartkówka <br>Data: 2019-01-13 (nd.)<br>Nauczyciel: Monińska Julita<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Monińska Julita<br/><br>Komentarz: Gerund vs Infinitive" class="ocena" href="/przegladaj_oceny/szczegoly/20378516" >5+</a></span><span id="Ocena94" class="grade-box" style="background-color:#66CDAA; ">
            <a title="Kategoria: prace pisemne<br>Data: 2019-01-13 (nd.)<br>Nauczyciel: Monińska Julita<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Monińska Julita<br/><br>Komentarz: Esej" class="ocena" href="/przegladaj_oceny/szczegoly/20377496" >6</a></span><span id="Ocena95" class="grade-box" style="background-color:#7CFC00; ">
            <a title="Kategoria: Projekt<br>Data: 2019-01-15 (wt.)<br>Nauczyciel: Monińska Julita<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Monińska Julita<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/20889530" >6</a></span><span id="Ocena96" class="grade-box" style="background-color:#F0E68C; ">
            <a title="Kategoria: sluchanie<br>Data: 2019-01-17 (czw.)<br>Nauczyciel: Monińska Julita<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Monińska Julita<br/><br>Komentarz: FCE Listening" class="ocena" href="/przegladaj_oceny/szczegoly/21123075" >6</a></span><span id="Ocena97" class="grade-box" style="background-color:#66CDAA; ">
            <a title="Kategoria: prace pisemne<br>Data: 2019-01-24 (czw.)<br>Nauczyciel: Monińska Julita<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Monińska Julita<br/><br>Komentarz: A letter of Application" class="ocena" href="/przegladaj_oceny/szczegoly/22064444" >6</a></span></td><td class="right">5.27</td><td class="center" > - </td><td class="center" ><span id="Ocena98" class="grade-box" style="background-color:#B0C4DE; ">
            <a title="Kategoria: śródroczna<br>Data: 2019-01-24 (czw.)<br>Nauczyciel: Monińska Julita<br>Licz do średniej: nie<br>Dodał: Monińska Julita<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/22064468" >6</a></span></td><td ><span id="Ocena99" class="grade-box" style="background-color:#FFD700; ">
            <a title="Kategoria: Pisanie-egzamin<br>Data: 2019-01-28 (pon.)<br>Nauczyciel: Monińska Julita<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Monińska Julita<br/><br>Komentarz: A Letter of application" class="ocena" href="/przegladaj_oceny/szczegoly/22429477" >6</a></span><span id="Ocena100" class="grade-box" style="background-color:#87CEFA; ">
            <a title="Kategoria: kartkówka <br>Data: 2019-02-01 (pt.)<br>Nauczyciel: Monińska Julita<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Monińska Julita<br/><br>Komentarz: Samodzielna prac z tekstem-praca domowa<br />
Ocena poprawiona z 2+" class="ocena" href="/przegladaj_oceny/szczegoly/22800112" >6-</a></span><span id="Ocena101" class="grade-box" style="background-color:#7B68EE; ">
            <a title="Ocena: nieprzygotowany<br>Kategoria: praca domowa<br>Data: 2019-02-05 (wt.)<br>Nauczyciel: Monińska Julita<br>Dodał: Monińska Julita<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/23041339" >np</a></span><span id="Ocena102" class="grade-box" style="background-color:#7CFC00; ">
            <a title="Kategoria: prezentacje<br>Data: 2019-02-08 (pt.)<br>Nauczyciel: Monińska Julita<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Monińska Julita<br/><br>Komentarz: Culture-The Events" class="ocena" href="/przegladaj_oceny/szczegoly/23436509" >5+</a></span><span id="Ocena103" class="grade-box" style="background-color:#7B68EE; ">
            <a title="Ocena: brak zadania<br>Kategoria: praca domowa<br>Data: 2019-03-05 (wt.)<br>Nauczyciel: Monińska Julita<br>Dodał: Monińska Julita<br/><br>Komentarz: Brak właściwego zadania" class="ocena" href="/przegladaj_oceny/szczegoly/26592452" >bz</a></span><span id="Ocena104" class="grade-box" style="background-color:#FF1493; ">
            <a title="Kategoria: sprawdzian<br>Data: 2019-03-08 (pt.)<br>Nauczyciel: Monińska Julita<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Monińska Julita<br/><br>Komentarz: Test Unit 8" class="ocena" href="/przegladaj_oceny/szczegoly/27386158" >4</a></span><span id="Ocena105" class="grade-box" style="background-color:#7B68EE; ">
            <a title="Kategoria: praca domowa<br>Data: 2019-03-12 (wt.)<br>Nauczyciel: Monińska Julita<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Monińska Julita<br/><br>Komentarz: Idiomy-shopping" class="ocena" href="/przegladaj_oceny/szczegoly/27645326" >5+</a></span><span id="Ocena106" class="grade-box" style="background-color:#E6E6FA; ">
            <a title="Kategoria: praca na lekcji(grupowa/indywidualna)<br>Data: 2019-03-14 (czw.)<br>Nauczyciel: Monińska Julita<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Monińska Julita<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/28155006" >6</a></span><span id="Ocena107" class="grade-box" style="background-color:#FFD700; ">
            <a title="Kategoria: Pisanie-egzamin<br>Data: 2019-03-17 (nd.)<br>Nauczyciel: Monińska Julita<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Monińska Julita<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/28600816" >6</a></span><span id="Ocena108" class="grade-box" style="background-color:#FFD700; ">
            <a title="Kategoria: Pisanie-egzamin<br>Data: 2019-03-25 (pon.)<br>Nauczyciel: Monińska Julita<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Monińska Julita<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/29892722" >6</a></span><span id="Ocena109" class="grade-box" style="background-color:#7B68EE; ">
            <a title="Kategoria: praca domowa<br>Data: 2019-03-29 (pt.)<br>Nauczyciel: Monińska Julita<br>Dodał: Monińska Julita<br/><br>Komentarz: Tłumaczenie fragmentów zdań" class="ocena" href="/przegladaj_oceny/szczegoly/30953461" >0</a></span><span id="Ocena110" class="grade-box" style="background-color:#FFD700; ">
            <a title="Kategoria: Pisanie-egzamin<br>Data: 2019-03-29 (pt.)<br>Nauczyciel: Monińska Julita<br>Dodał: Monińska Julita<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/30952835" >0</a></span><span id="Ocena111" class="grade-box" style="background-color:#87CEFA; ">
            <a title="Kategoria: kartkówka <br>Data: 2019-03-29 (pt.)<br>Nauczyciel: Monińska Julita<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Monińska Julita<br/><br>Komentarz: Modals" class="ocena" href="/przegladaj_oceny/szczegoly/30804564" >5+</a></span></td><td  class="right">5.55</td><td class="center"  > - </td><td  class="right"  >5.37</td><td class="center"  > - </td><td class="center"  > - </td></tr><tr class="line1" name="przedmioty_all" id="przedmioty_32782" style="display: none; visibility: hidden;">
                              <td colspan="14" ><table class="stretch"><thead><tr class=""><td >Ocena</td><td title="Komentarz do oceny">K</td><td>Kategoria</td><td class="center">Data</td><td>Nauczyciel</td><td>Licz do śr.</td><td>Waga</td><td >Poprawa oceny</td><td>Dodał</td></tr></thead><tbody><tr class="line1 bolded"><td colspan="9" class="center">Okres 1</td></tr><tr class="line1 detail-grades" style="background-color: #E6E6FA;"><td class="center">5</td><td class="center">&nbsp;</td><td>praca na lekcji(grupowa/indywidualna)</td><td class="center">2018-09-06</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #87CEFA;"><td class="center">4+</td><td class="center">&nbsp;</td><td>kartkówka </td><td class="center">2018-09-18</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #87CEFA;"><td class="center">5</td><td class="center">&nbsp;</td><td>kartkówka </td><td class="center">2018-09-27</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #7B68EE;"><td class="center">5</td><td class="center">&nbsp;</td><td>praca domowa</td><td class="center">2018-10-07</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #FFA07A;"><td class="center">6</td><td class="center">&nbsp;</td><td>Speech</td><td class="center">2018-10-09</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #7B68EE;"><td class="center">4</td><td class="center">&nbsp;</td><td>praca domowa</td><td class="center">2018-10-11</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #ADFF2F;"><td class="center">+</td><td class="center">&nbsp;</td><td>aktywność</td><td class="center">2018-10-11</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #66CDAA;"><td class="center">6-</td><td class="center">&nbsp;</td><td>prace pisemne</td><td class="center">2018-10-28</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #FF1493;"><td class="center">4-</td><td class="center">&nbsp;</td><td>sprawdzian</td><td class="center">2018-10-30</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #7CFC00;"><td class="center">6</td><td class="center">&nbsp;</td><td>prezentacje</td><td class="center">2018-10-31</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #87CEFA;"><td class="center">5+</td><td class="center">&nbsp;</td><td>kartkówka </td><td class="center">2018-11-15</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #F0E68C;"><td class="center">6</td><td class="center">&nbsp;</td><td>sluchanie</td><td class="center">2018-11-23</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #F0E68C;"><td class="center">6</td><td class="center">&nbsp;</td><td>aktywność</td><td class="center">2018-12-10</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #FFD700;"><td class="center">2</td><td class="center">&nbsp;</td><td>Pisanie-egzamin</td><td class="center">2019-01-06</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #87CEFA;"><td class="center">5+</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/20378516','ko',480,450)" >K</a></strong></td><td>kartkówka </td><td class="center">2019-01-13</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #66CDAA;"><td class="center">6</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/20377496','ko',480,450)" >K</a></strong></td><td>prace pisemne</td><td class="center">2019-01-13</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #7CFC00;"><td class="center">6</td><td class="center">&nbsp;</td><td>Projekt</td><td class="center">2019-01-15</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #F0E68C;"><td class="center">6</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/21123075','ko',480,450)" >K</a></strong></td><td>sluchanie</td><td class="center">2019-01-17</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #B0C4DE;"><td class="center">6</td><td class="center">&nbsp;</td><td>śródroczna</td><td class="center">2019-01-24</td><td >Monińska Julita</td><td class="center"><img src="/images/nieaktywne.png"  border="0" /></td><td class="right" ></td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #66CDAA;"><td class="center">6</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/22064444','ko',480,450)" >K</a></strong></td><td>prace pisemne</td><td class="center">2019-01-24</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 bolded"><td colspan="9" class="center">Okres 2</td></tr><tr class="line1 detail-grades" style="background-color: #FFD700;"><td class="center">6</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/22429477','ko',480,450)" >K</a></strong></td><td>Pisanie-egzamin</td><td class="center">2019-01-28</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #87CEFA;"><td class="center">6-</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/22800112','ko',480,450)" >K</a></strong></td><td>kartkówka </td><td class="center">2019-02-01</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #7B68EE;"><td class="center">np</td><td class="center">&nbsp;</td><td>praca domowa</td><td class="center">2019-02-05</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #7CFC00;"><td class="center">5+</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/23436509','ko',480,450)" >K</a></strong></td><td>prezentacje</td><td class="center">2019-02-08</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #7B68EE;"><td class="center">bz</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/26592452','ko',480,450)" >K</a></strong></td><td>praca domowa</td><td class="center">2019-03-05</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #FF1493;"><td class="center">4</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/27386158','ko',480,450)" >K</a></strong></td><td>sprawdzian</td><td class="center">2019-03-08</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #7B68EE;"><td class="center">5+</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/27645326','ko',480,450)" >K</a></strong></td><td>praca domowa</td><td class="center">2019-03-12</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #E6E6FA;"><td class="center">6</td><td class="center">&nbsp;</td><td>praca na lekcji(grupowa/indywidualna)</td><td class="center">2019-03-14</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #FFD700;"><td class="center">6</td><td class="center">&nbsp;</td><td>Pisanie-egzamin</td><td class="center">2019-03-17</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #FFD700;"><td class="center">6</td><td class="center">&nbsp;</td><td>Pisanie-egzamin</td><td class="center">2019-03-25</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #7B68EE;"><td class="center">0</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/30953461','ko',480,450)" >K</a></strong></td><td>praca domowa</td><td class="center">2019-03-29</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #FFD700;"><td class="center">0</td><td class="center">&nbsp;</td><td>Pisanie-egzamin</td><td class="center">2019-03-29</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Monińska Julita</td></tr><tr class="line1 detail-grades" style="background-color: #87CEFA;"><td class="center">5+</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/30804564','ko',480,450)" >K</a></strong></td><td>kartkówka </td><td class="center">2019-03-29</td><td >Monińska Julita</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Monińska Julita</td></tr></tbody><tfoot><tr><td  class="center" colspan="14"></td></tr></tfoot></table></td></tr><tr class="line0">
                            <td class='center micro screen-only'><img src="/images/tree_colapsed.png" id="przedmioty_32783_node" onclick="showHide.ShowHide(32783);" /></td>
                            <td >Język niemiecki</td><td ><span id="Ocena112" class="grade-box" style="background-color:#FFEBCD; ">
            <a title="Kategoria: inna<br>Data: 2018-10-11 (czw.)<br>Nauczyciel: Kozłow Marek<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Kozłow Marek<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/4315959" >5</a></span><span id="Ocena113" class="grade-box" style="background-color:#DC143C; ">
            <a title="Kategoria: praca klasowa<br>Data: 2018-10-23 (wt.)<br>Nauczyciel: Kozłow Marek<br>Dodał: Kozłow Marek<br/>Obowiązek wyk. zadania: TAK<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/6145518" >-</a></span><span id="Ocena114" class="grade-box" style="background-color:#BA55D3; ">
            <a title="Kategoria: Wypowiedż<br>Data: 2018-11-28 (śr.)<br>Nauczyciel: Kozłow Marek<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Kozłow Marek<br/>Obowiązek wyk. zadania: TAK<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/11757764" >6-</a></span><span id="Ocena115" class="grade-box" style="background-color:#FFEBCD; ">
            <a title="Kategoria: inna<br>Data: 2018-12-19 (śr.)<br>Nauczyciel: Kozłow Marek<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Kozłow Marek<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/16444985" >4+</a></span><span id="Ocena116" class="grade-box" style="background-color:#F0E68C; ">
            <a title="Kategoria: zadanie<br>Data: 2019-01-11 (pt.)<br>Nauczyciel: Kozłow Marek<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Kozłow Marek<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/20008908" >6</a></span><span id="Ocena117" class="grade-box" style="background-color:#DC143C; ">
            <a title="Kategoria: praca klasowa<br>Data: 2019-01-23 (śr.)<br>Nauczyciel: Kozłow Marek<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Kozłow Marek<br/>Obowiązek wyk. zadania: TAK<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/21984385" >5</a></span></td><td class="right">5.31</td><td class="center" > - </td><td class="center" ><span id="Ocena118" class="grade-box" style="background-color:#B0C4DE; ">
            <a title="Kategoria: śródroczna<br>Data: 2019-01-23 (śr.)<br>Nauczyciel: Kozłow Marek<br>Licz do średniej: nie<br>Dodał: Kozłow Marek<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/21985031" >5</a></span></td><td ><span id="Ocena119" class="grade-box" style="background-color:#DC143C; ">
            <a title="Kategoria: praca klasowa<br>Data: 2019-03-26 (wt.)<br>Nauczyciel: Kozłow Marek<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Kozłow Marek<br/>Obowiązek wyk. zadania: TAK<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/30170708" >4</a></span></td><td  class="right">4.00</td><td class="center"  > - </td><td  class="right"  >4.98</td><td class="center"  > - </td><td class="center"  > - </td></tr><tr class="line0" name="przedmioty_all" id="przedmioty_32783" style="display: none; visibility: hidden;">
                              <td colspan="14" ><table class="stretch"><thead><tr class=""><td >Ocena</td><td title="Komentarz do oceny">K</td><td>Kategoria</td><td class="center">Data</td><td>Nauczyciel</td><td>Licz do śr.</td><td>Waga</td><td >Poprawa oceny</td><td>Dodał</td></tr></thead><tbody><tr class="line1 bolded"><td colspan="9" class="center">Okres 1</td></tr><tr class="line1 detail-grades" style="background-color: #FFEBCD;"><td class="center">5</td><td class="center">&nbsp;</td><td>inna</td><td class="center">2018-10-11</td><td >Kozłow Marek</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Kozłow Marek</td></tr><tr class="line1 detail-grades" style="background-color: #DC143C;"><td class="center">-</td><td class="center">&nbsp;</td><td>praca klasowa</td><td class="center">2018-10-23</td><td >Kozłow Marek</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Kozłow Marek</td></tr><tr class="line1 detail-grades" style="background-color: #BA55D3;"><td class="center">6-</td><td class="center">&nbsp;</td><td>Wypowiedż</td><td class="center">2018-11-28</td><td >Kozłow Marek</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Kozłow Marek</td></tr><tr class="line1 detail-grades" style="background-color: #FFEBCD;"><td class="center">4+</td><td class="center">&nbsp;</td><td>inna</td><td class="center">2018-12-19</td><td >Kozłow Marek</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Kozłow Marek</td></tr><tr class="line1 detail-grades" style="background-color: #F0E68C;"><td class="center">6</td><td class="center">&nbsp;</td><td>zadanie</td><td class="center">2019-01-11</td><td >Kozłow Marek</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Kozłow Marek</td></tr><tr class="line1 detail-grades" style="background-color: #B0C4DE;"><td class="center">5</td><td class="center">&nbsp;</td><td>śródroczna</td><td class="center">2019-01-23</td><td >Kozłow Marek</td><td class="center"><img src="/images/nieaktywne.png"  border="0" /></td><td class="right" ></td><td class="center" >-</td><td >Kozłow Marek</td></tr><tr class="line1 detail-grades" style="background-color: #DC143C;"><td class="center">5</td><td class="center">&nbsp;</td><td>praca klasowa</td><td class="center">2019-01-23</td><td >Kozłow Marek</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Kozłow Marek</td></tr><tr class="line1 bolded"><td colspan="9" class="center">Okres 2</td></tr><tr class="line1 detail-grades" style="background-color: #DC143C;"><td class="center">4</td><td class="center">&nbsp;</td><td>praca klasowa</td><td class="center">2019-03-26</td><td >Kozłow Marek</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Kozłow Marek</td></tr></tbody><tfoot><tr><td  class="center" colspan="14"></td></tr></tfoot></table></td></tr><tr class="line1">
                            <td class='center micro screen-only'><img src="/images/tree_colapsed.png" id="przedmioty_32784_node" onclick="showHide.ShowHide(32784);" /></td>
                            <td >Język polski</td><td ><span id="Ocena120" class="grade-box" style="background-color:#FF8C00; ">
            <a title="Kategoria: wywiady<br>Data: 2018-10-02 (wt.)<br>Nauczyciel: Szymańska Katarzyna<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Szymańska Katarzyna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/2597408" >6-</a></span><span id="Ocena121" class="grade-box" style="background-color:#A9A9A9; ">
            <a title="Kategoria: aktywność<br>Data: 2018-10-18 (czw.)<br>Nauczyciel: Szymańska Katarzyna<br>Dodał: Szymańska Katarzyna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/4863208" >+</a></span><span id="Ocena122" class="grade-box" style="background-color:#A9A9A9; ">
            <a title="Kategoria: aktywność<br>Data: 2018-10-18 (czw.)<br>Nauczyciel: Szymańska Katarzyna<br>Dodał: Szymańska Katarzyna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/4863205" >+</a></span><span id="Ocena123" class="grade-box" style="background-color:#32CD32; ">
            <a title="Kategoria: prace z testów<br>Data: 2018-10-22 (pon.)<br>Nauczyciel: Szymańska Katarzyna<br>Licz do średniej: tak<br>Waga: 4<br>Dodał: Szymańska Katarzyna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/5904115" >4</a></span><span id="Ocena124" class="grade-box" style="background-color:#7CFC00; ">
            <a title="Kategoria: obowiązkowa praca na dowolny temet<br>Data: 2018-11-05 (pon.)<br>Nauczyciel: Szymańska Katarzyna<br>Licz do średniej: tak<br>Waga: 4<br>Dodał: Szymańska Katarzyna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/7951469" >5-</a></span><span id="Ocena125" class="grade-box" style="background-color:#7CFC00; ">
            <a title="Kategoria: obowiązkowa praca na dowolny temet<br>Data: 2018-11-05 (pon.)<br>Nauczyciel: Szymańska Katarzyna<br>Licz do średniej: tak<br>Waga: 4<br>Dodał: Szymańska Katarzyna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/7951468" >4-</a></span><span id="Ocena126" class="grade-box" style="background-color:#FF1493; ">
            <a title="Kategoria: projekt<br>Data: 2018-11-16 (pt.)<br>Nauczyciel: Szymańska Katarzyna<br>Licz do średniej: tak<br>Waga: 5<br>Dodał: Szymańska Katarzyna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/9823737" >6</a></span><span id="Ocena127" class="grade-box" style="background-color:#7CFC00; ">
            <a title="Kategoria: obowiązkowa praca na dowolny temet<br>Data: 2018-11-22 (czw.)<br>Nauczyciel: Szymańska Katarzyna<br>Licz do średniej: tak<br>Waga: 4<br>Dodał: Szymańska Katarzyna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/10857081" >6</a></span><span id="Ocena128" class="grade-box" style="background-color:#3333FF; ">
            <a title="Kategoria: test<br>Data: 2018-11-29 (czw.)<br>Nauczyciel: Szymańska Katarzyna<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Szymańska Katarzyna<br/><br>Komentarz: 18/30 + 5/10 = 23/40 58%" class="ocena" href="/przegladaj_oceny/szczegoly/12019747" >3+</a></span><span id="Ocena129" class="grade-box" style="background-color:#A9A9A9; ">
            <a title="Kategoria: aktywność<br>Data: 2018-11-29 (czw.)<br>Nauczyciel: Szymańska Katarzyna<br>Dodał: Szymańska Katarzyna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/11520700" >+</a></span><span id="Ocena130" class="grade-box" style="background-color:#A9A9A9; ">
            <a title="Kategoria: aktywność<br>Data: 2018-11-29 (czw.)<br>Nauczyciel: Szymańska Katarzyna<br>Dodał: Szymańska Katarzyna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/11520698" >+</a></span><span id="Ocena131" class="grade-box" style="background-color:#E6E6FA; ">
            <a title="Kategoria: rozprawka<br>Data: 2018-12-10 (pon.)<br>Nauczyciel: Szymańska Katarzyna<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Szymańska Katarzyna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/14051557" >5-</a></span><span id="Ocena132" class="grade-box" style="background-color:#7CFC00; ">
            <a title="Kategoria: obowiązkowa praca na dowolny temet<br>Data: 2019-01-18 (pt.)<br>Nauczyciel: Szymańska Katarzyna<br>Licz do średniej: tak<br>Waga: 4<br>Dodał: Szymańska Katarzyna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/21429878" >4+</a></span></td><td class="right">4.77</td><td class="center" > - </td><td class="center" ><span id="Ocena133" class="grade-box" style="background-color:#B0C4DE; ">
            <a title="Kategoria: śródroczna<br>Data: 2019-01-24 (czw.)<br>Nauczyciel: Szymańska Katarzyna<br>Licz do średniej: nie<br>Dodał: Szymańska Katarzyna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/22107367" >5-</a></span></td><td ><span id="Ocena134" class="grade-box" style="background-color:#3333FF; ">
            <a title="Kategoria: test<br>Data: 2019-01-28 (pon.)<br>Nauczyciel: Szymańska Katarzyna<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Szymańska Katarzyna<br/><br>Komentarz: 18/24 + 10/10 = 28/34 82%" class="ocena" href="/przegladaj_oceny/szczegoly/22392025" >4+</a></span><span id="Ocena135" class="grade-box" style="background-color:#FF1493; ">
            <a title="Kategoria: projekt<br>Data: 2019-03-11 (pon.)<br>Nauczyciel: Szymańska Katarzyna<br>Licz do średniej: tak<br>Waga: 5<br>Dodał: Szymańska Katarzyna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/27562780" >6</a></span><span id="Ocena136" class="grade-box" style="background-color:#3333FF; ">
            <a title="Kategoria: test<br>Data: 2019-03-18 (pon.)<br>Nauczyciel: Szymańska Katarzyna<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Szymańska Katarzyna<br/><br>Komentarz: 21/24 + 7/10 = 28/34 82%" class="ocena" href="/przegladaj_oceny/szczegoly/28789593" >4+</a></span><span id="Ocena137" class="grade-box" style="background-color:#7CFC00; ">
            <a title="Kategoria: obowiązkowa praca na dowolny temet<br>Data: 2019-03-26 (wt.)<br>Nauczyciel: Szymańska Katarzyna<br>Licz do średniej: tak<br>Waga: 4<br>Dodał: Szymańska Katarzyna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/30100065" >6</a></span><span id="Ocena138" class="grade-box" style="background-color:#3333FF; ">
            <a title="Kategoria: test<br>Data: 2019-04-05 (pt.)<br>Nauczyciel: Szymańska Katarzyna<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Szymańska Katarzyna<br/><br>Komentarz: 15/22 + 2/2 + 10/10 = 27/34 79%" class="ocena" href="/przegladaj_oceny/szczegoly/32039368" >4</a></span></td><td  class="right">5.17</td><td class="center"  > - </td><td  class="right"  >4.92</td><td class="center"  ><span id="Ocena139" class="grade-box" style="background-color:#F0FFFF; ">
            <a title="Kategoria: przewidywana roczna<br>Data: 2019-04-25 (czw.)<br>Nauczyciel: Szymańska Katarzyna<br>Licz do średniej: nie<br>Dodał: Szymańska Katarzyna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/32657949" >6</a></span></td><td class="center"  > - </td></tr><tr class="line1" name="przedmioty_all" id="przedmioty_32784" style="display: none; visibility: hidden;">
                              <td colspan="14" ><table class="stretch"><thead><tr class=""><td >Ocena</td><td title="Komentarz do oceny">K</td><td>Kategoria</td><td class="center">Data</td><td>Nauczyciel</td><td>Licz do śr.</td><td>Waga</td><td >Poprawa oceny</td><td>Dodał</td></tr></thead><tbody><tr class="line1 bolded"><td colspan="9" class="center">Okres 1</td></tr><tr class="line1 detail-grades" style="background-color: #FF8C00;"><td class="center">6-</td><td class="center">&nbsp;</td><td>wywiady</td><td class="center">2018-10-02</td><td >Szymańska Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Szymańska Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #A9A9A9;"><td class="center">+</td><td class="center">&nbsp;</td><td>aktywność</td><td class="center">2018-10-18</td><td >Szymańska Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Szymańska Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #A9A9A9;"><td class="center">+</td><td class="center">&nbsp;</td><td>aktywność</td><td class="center">2018-10-18</td><td >Szymańska Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Szymańska Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #32CD32;"><td class="center">4</td><td class="center">&nbsp;</td><td>prace z testów</td><td class="center">2018-10-22</td><td >Szymańska Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >4</td><td class="center" >-</td><td >Szymańska Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #7CFC00;"><td class="center">5-</td><td class="center">&nbsp;</td><td>obowiązkowa praca na dowolny temet</td><td class="center">2018-11-05</td><td >Szymańska Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >4</td><td class="center" >-</td><td >Szymańska Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #7CFC00;"><td class="center">4-</td><td class="center">&nbsp;</td><td>obowiązkowa praca na dowolny temet</td><td class="center">2018-11-05</td><td >Szymańska Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >4</td><td class="center" >-</td><td >Szymańska Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #FF1493;"><td class="center">6</td><td class="center">&nbsp;</td><td>projekt</td><td class="center">2018-11-16</td><td >Szymańska Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >5</td><td class="center" >-</td><td >Szymańska Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #7CFC00;"><td class="center">6</td><td class="center">&nbsp;</td><td>obowiązkowa praca na dowolny temet</td><td class="center">2018-11-22</td><td >Szymańska Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >4</td><td class="center" >-</td><td >Szymańska Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #3333FF;"><td class="center">3+</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/12019747','ko',480,450)" >K</a></strong></td><td>test</td><td class="center">2018-11-29</td><td >Szymańska Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Szymańska Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #A9A9A9;"><td class="center">+</td><td class="center">&nbsp;</td><td>aktywność</td><td class="center">2018-11-29</td><td >Szymańska Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Szymańska Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #A9A9A9;"><td class="center">+</td><td class="center">&nbsp;</td><td>aktywność</td><td class="center">2018-11-29</td><td >Szymańska Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Szymańska Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #E6E6FA;"><td class="center">5-</td><td class="center">&nbsp;</td><td>rozprawka</td><td class="center">2018-12-10</td><td >Szymańska Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Szymańska Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #7CFC00;"><td class="center">4+</td><td class="center">&nbsp;</td><td>obowiązkowa praca na dowolny temet</td><td class="center">2019-01-18</td><td >Szymańska Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >4</td><td class="center" >-</td><td >Szymańska Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #B0C4DE;"><td class="center">5-</td><td class="center">&nbsp;</td><td>śródroczna</td><td class="center">2019-01-24</td><td >Szymańska Katarzyna</td><td class="center"><img src="/images/nieaktywne.png"  border="0" /></td><td class="right" ></td><td class="center" >-</td><td >Szymańska Katarzyna</td></tr><tr class="line1 bolded"><td colspan="9" class="center">Okres 2</td></tr><tr class="line1 detail-grades" style="background-color: #3333FF;"><td class="center">4+</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/22392025','ko',480,450)" >K</a></strong></td><td>test</td><td class="center">2019-01-28</td><td >Szymańska Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Szymańska Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #FF1493;"><td class="center">6</td><td class="center">&nbsp;</td><td>projekt</td><td class="center">2019-03-11</td><td >Szymańska Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >5</td><td class="center" >-</td><td >Szymańska Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #3333FF;"><td class="center">4+</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/28789593','ko',480,450)" >K</a></strong></td><td>test</td><td class="center">2019-03-18</td><td >Szymańska Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Szymańska Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #7CFC00;"><td class="center">6</td><td class="center">&nbsp;</td><td>obowiązkowa praca na dowolny temet</td><td class="center">2019-03-26</td><td >Szymańska Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >4</td><td class="center" >-</td><td >Szymańska Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #3333FF;"><td class="center">4</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/32039368','ko',480,450)" >K</a></strong></td><td>test</td><td class="center">2019-04-05</td><td >Szymańska Katarzyna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Szymańska Katarzyna</td></tr><tr class="line1 detail-grades" style="background-color: #F0FFFF;"><td class="center">6</td><td class="center">&nbsp;</td><td>przewidywana roczna</td><td class="center">2019-04-25</td><td >Szymańska Katarzyna</td><td class="center"><img src="/images/nieaktywne.png"  border="0" /></td><td class="right" ></td><td class="center" >-</td><td >Szymańska Katarzyna</td></tr></tbody><tfoot><tr><td  class="center" colspan="14"></td></tr></tfoot></table></td></tr><tr class="line0">
                            <td class='center micro screen-only'><img src="/images/tree_colapsed.png" id="przedmioty_32785_node" onclick="showHide.ShowHide(32785);" /></td>
                            <td >Matematyka</td><td ><span id="Ocena140" class="grade-box" style="background-color:#32CD32; ">
            <a title="Kategoria: kartkówka<br>Data: 2018-10-12 (pt.)<br>Nauczyciel: Nadolska Monika<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Nadolska Monika<br/><br>Komentarz: pojęcie funkcji" class="ocena" href="/przegladaj_oceny/szczegoly/7221123" >4-</a></span><span id="Ocena141" class="grade-box" style="background-color:#DC143C; ">
            <a title="Kategoria: Praca klasowa<br>Data: 2018-10-15 (pon.)<br>Nauczyciel: Nadolska Monika<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Nadolska Monika<br/><br>Komentarz: Sesja z plusem 1" class="ocena" href="/przegladaj_oceny/szczegoly/4551749" >4</a></span><span id="Ocena142" class="grade-box" style="background-color:#ADFF2F; ">
            <a title="Kategoria: aktywność<br>Data: 2018-10-22 (pon.)<br>Nauczyciel: Nadolska Monika<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Nadolska Monika<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/5918983" >6</a></span><span id="Ocena143" class="grade-box" style="background-color:#ADFF2F; ">
            <a title="Kategoria: aktywność<br>Data: 2018-10-23 (wt.)<br>Nauczyciel: Nadolska Monika<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Nadolska Monika<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/6028815" >6</a></span><span id="Ocena144" class="grade-box" style="background-color:#32CD32; ">
            <a title="Kategoria: kartkówka<br>Data: 2018-11-02 (pt.)<br>Nauczyciel: Nadolska Monika<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Nadolska Monika<br/><br>Komentarz: wzajemne położenie dwóch okręgów" class="ocena" href="/przegladaj_oceny/szczegoly/12704848" >5</a></span><span>[<span id="Ocena145" class="grade-box" style="background-color:#FF8C00; ">
            <a title="Kategoria: spr (oc. poprawiona)<br>Data: 2018-11-06 (wt.)<br>Nauczyciel: Nadolska Monika<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Nadolska Monika<br/><br>Komentarz: Sprawdzian z kalendarza gimnazjalisty - Liczby" class="ocena" href="/przegladaj_oceny/szczegoly/8043486" >3</a></span><span id="Ocena146" class="grade-box" style="background-color:#FFA07A; ">
            <a title="Kategoria: Sprawdzian<br>Data: 2018-11-15 (czw.)<br>Nauczyciel: Nadolska Monika<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Nadolska Monika<br/>&lt;br /&gt;Poprawa oceny: 3 (spr (oc. poprawiona))" class="ocena" href="/przegladaj_oceny/szczegoly/9524333" >5</a></span>]</span><span>[<span id="Ocena147" class="grade-box" style="background-color:#FF8C00; ">
            <a title="Kategoria: pk poprawiona<br>Data: 2018-11-06 (wt.)<br>Nauczyciel: Nadolska Monika<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Nadolska Monika<br/>Obowiązek wyk. zadania: TAK<br/><br>Komentarz: Praca klasowa - liczby i wyrażenia algebraiczne" class="ocena" href="/przegladaj_oceny/szczegoly/8043434" >3</a></span><span id="Ocena148" class="grade-box" style="background-color:#DC143C; ">
            <a title="Kategoria: Praca klasowa<br>Data: 2018-11-29 (czw.)<br>Nauczyciel: Nadolska Monika<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Nadolska Monika<br/>&lt;br /&gt;Poprawa oceny: 3 (pk poprawiona)" class="ocena" href="/przegladaj_oceny/szczegoly/12011574" >4-</a></span>]</span><span id="Ocena149" class="grade-box" style="background-color:#66CDAA; ">
            <a title="Kategoria: Praca na lekcji<br>Data: 2018-11-20 (wt.)<br>Nauczyciel: Nadolska Monika<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Nadolska Monika<br/><br>Komentarz: trójkąty" class="ocena" href="/przegladaj_oceny/szczegoly/10266357" >5</a></span><span id="Ocena150" class="grade-box" style="background-color:#F0E68C; ">
            <a title="Kategoria: Gimplus<br>Data: 2018-11-25 (nd.)<br>Nauczyciel: Nadolska Monika<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Nadolska Monika<br/><br>Komentarz: Gimplus - funkcje" class="ocena" href="/przegladaj_oceny/szczegoly/11191915" >5+</a></span><span id="Ocena151" class="grade-box" style="background-color:#DC143C; ">
            <a title="Kategoria: Praca klasowa<br>Data: 2018-11-27 (wt.)<br>Nauczyciel: Nadolska Monika<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Nadolska Monika<br/><br>Komentarz: Funkcje" class="ocena" href="/przegladaj_oceny/szczegoly/11480723" >6-</a></span><span id="Ocena152" class="grade-box" style="background-color:#ADFF2F; ">
            <a title="Kategoria: aktywność<br>Data: 2018-12-04 (wt.)<br>Nauczyciel: Nadolska Monika<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Nadolska Monika<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/12778947" >6</a></span><span>[<span id="Ocena153" class="grade-box" style="background-color:#FF8C00; ">
            <a title="Kategoria: spr (oc. poprawiona)<br>Data: 2018-12-11 (wt.)<br>Nauczyciel: Nadolska Monika<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Nadolska Monika<br/><br>Komentarz: sprawdzian z kalendarza gimnazjalisty - algebra (wyrażenia algebraiczne, równania, układy równań)" class="ocena" href="/przegladaj_oceny/szczegoly/14177968" >2+</a></span><span id="Ocena154" class="grade-box" style="background-color:#FFA07A; ">
            <a title="Kategoria: Sprawdzian<br>Data: 2019-01-15 (wt.)<br>Nauczyciel: Nadolska Monika<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Nadolska Monika<br/>&lt;br /&gt;Poprawa oceny: 2+ (spr (oc. poprawiona))" class="ocena" href="/przegladaj_oceny/szczegoly/20671039" >5+</a></span>]</span><span id="Ocena155" class="grade-box" style="background-color:#DC143C; ">
            <a title="Kategoria: Praca klasowa<br>Data: 2019-01-02 (śr.)<br>Nauczyciel: Nadolska Monika<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Nadolska Monika<br/><br>Komentarz: Figury na płaszczyźnie" class="ocena" href="/przegladaj_oceny/szczegoly/17455093" >6</a></span><span id="Ocena156" class="grade-box" style="background-color:#FFA07A; ">
            <a title="Kategoria: Sprawdzian<br>Data: 2019-01-15 (wt.)<br>Nauczyciel: Nadolska Monika<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Nadolska Monika<br/><br>Komentarz: Kalendarz gimnazjalisty - funkcje" class="ocena" href="/przegladaj_oceny/szczegoly/20665303" >5+</a></span><span id="Ocena157" class="grade-box" style="background-color:#DC143C; ">
            <a title="Kategoria: Praca klasowa<br>Data: 2019-01-18 (pt.)<br>Nauczyciel: Nadolska Monika<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Nadolska Monika<br/><br>Komentarz: Sesja z plusem 2" class="ocena" href="/przegladaj_oceny/szczegoly/21424519" >6</a></span><span id="Ocena158" class="grade-box" style="background-color:#ADFF2F; ">
            <a title="Kategoria: aktywność<br>Data: 2019-01-25 (pt.)<br>Nauczyciel: Nadolska Monika<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Nadolska Monika<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/22253680" >6</a></span></td><td class="right">4.77</td><td class="center" > - </td><td class="center" ><span id="Ocena159" class="grade-box" style="background-color:#B0C4DE; ">
            <a title="Kategoria: śródroczna<br>Data: 2019-01-21 (pon.)<br>Nauczyciel: Nadolska Monika<br>Licz do średniej: nie<br>Dodał: Nadolska Monika<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/21790575" >5-</a></span></td><td ><span id="Ocena160" class="grade-box" style="background-color:#FFA07A; ">
            <a title="Kategoria: Sprawdzian<br>Data: 2019-02-25 (pon.)<br>Nauczyciel: Nadolska Monika<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Nadolska Monika<br/><br>Komentarz: Figury podobne" class="ocena" href="/przegladaj_oceny/szczegoly/25253908" >6</a></span><span id="Ocena161" class="grade-box" style="background-color:#ADFF2F; ">
            <a title="Kategoria: aktywność<br>Data: 2019-03-01 (pt.)<br>Nauczyciel: Nadolska Monika<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Nadolska Monika<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/25934992" >6</a></span><span id="Ocena162" class="grade-box" style="background-color:#32CD32; ">
            <a title="Kategoria: kartkówka<br>Data: 2019-03-04 (pon.)<br>Nauczyciel: Nadolska Monika<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Nadolska Monika<br/><br>Komentarz: stożek" class="ocena" href="/przegladaj_oceny/szczegoly/26318073" >6</a></span><span id="Ocena163" class="grade-box" style="background-color:#32CD32; ">
            <a title="Kategoria: kartkówka<br>Data: 2019-03-12 (wt.)<br>Nauczyciel: Nadolska Monika<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Nadolska Monika<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/27665905" >6</a></span><span id="Ocena164" class="grade-box" style="background-color:#ADFF2F; ">
            <a title="Kategoria: aktywność<br>Data: 2019-03-21 (czw.)<br>Nauczyciel: Nadolska Monika<br>Dodał: Nadolska Monika<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/29632221" >+</a></span><span id="Ocena165" class="grade-box" style="background-color:#ADFF2F; ">
            <a title="Kategoria: aktywność<br>Data: 2019-03-22 (pt.)<br>Nauczyciel: Nadolska Monika<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Nadolska Monika<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/29632326" >6</a></span><span id="Ocena166" class="grade-box" style="background-color:#ADFF2F; ">
            <a title="Kategoria: aktywność<br>Data: 2019-04-01 (pon.)<br>Nauczyciel: Nadolska Monika<br>Dodał: Nadolska Monika<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/31164474" >+</a></span><span id="Ocena167" class="grade-box" style="background-color:#FFA07A; ">
            <a title="Kategoria: Sprawdzian<br>Data: 2019-04-03 (śr.)<br>Nauczyciel: Nadolska Monika<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Nadolska Monika<br/><br>Komentarz: Kalendarz gimnazjalisty - geometria" class="ocena" href="/przegladaj_oceny/szczegoly/31619566" >6</a></span></td><td  class="right">6.00</td><td class="center"  > - </td><td  class="right"  >4.99</td><td class="center"  > - </td><td class="center"  > - </td></tr><tr class="line0" name="przedmioty_all" id="przedmioty_32785" style="display: none; visibility: hidden;">
                              <td colspan="14" ><table class="stretch"><thead><tr class=""><td >Ocena</td><td title="Komentarz do oceny">K</td><td>Kategoria</td><td class="center">Data</td><td>Nauczyciel</td><td>Licz do śr.</td><td>Waga</td><td >Poprawa oceny</td><td>Dodał</td></tr></thead><tbody><tr class="line1 bolded"><td colspan="9" class="center">Okres 1</td></tr><tr class="line1 detail-grades" style="background-color: #32CD32;"><td class="center">4-</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/7221123','ko',480,450)" >K</a></strong></td><td>kartkówka</td><td class="center">2018-10-12</td><td >Nadolska Monika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Nadolska Monika</td></tr><tr class="line1 detail-grades" style="background-color: #DC143C;"><td class="center">4</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/4551749','ko',480,450)" >K</a></strong></td><td>Praca klasowa</td><td class="center">2018-10-15</td><td >Nadolska Monika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Nadolska Monika</td></tr><tr class="line1 detail-grades" style="background-color: #ADFF2F;"><td class="center">6</td><td class="center">&nbsp;</td><td>aktywność</td><td class="center">2018-10-22</td><td >Nadolska Monika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Nadolska Monika</td></tr><tr class="line1 detail-grades" style="background-color: #ADFF2F;"><td class="center">6</td><td class="center">&nbsp;</td><td>aktywność</td><td class="center">2018-10-23</td><td >Nadolska Monika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Nadolska Monika</td></tr><tr class="line1 detail-grades" style="background-color: #32CD32;"><td class="center">5</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/12704848','ko',480,450)" >K</a></strong></td><td>kartkówka</td><td class="center">2018-11-02</td><td >Nadolska Monika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Nadolska Monika</td></tr><tr class="line1 detail-grades" style="background-color: #FF8C00;"><td class="center">3</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/8043486','ko',480,450)" >K</a></strong></td><td>spr (oc. poprawiona)</td><td class="center">2018-11-06</td><td >Nadolska Monika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Nadolska Monika</td></tr><tr class="line1 detail-grades" style="background-color: #FFA07A;"><td class="center">5</td><td class="center">&nbsp;</td><td>Sprawdzian</td><td class="center">2018-11-15</td><td >Nadolska Monika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >3 (spr (oc. poprawiona))</td><td >Nadolska Monika</td></tr><tr class="line1 detail-grades" style="background-color: #FF8C00;"><td class="center">3</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/8043434','ko',480,450)" >K</a></strong></td><td>pk poprawiona</td><td class="center">2018-11-06</td><td >Nadolska Monika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Nadolska Monika</td></tr><tr class="line1 detail-grades" style="background-color: #DC143C;"><td class="center">4-</td><td class="center">&nbsp;</td><td>Praca klasowa</td><td class="center">2018-11-29</td><td >Nadolska Monika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >3 (pk poprawiona)</td><td >Nadolska Monika</td></tr><tr class="line1 detail-grades" style="background-color: #66CDAA;"><td class="center">5</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/10266357','ko',480,450)" >K</a></strong></td><td>Praca na lekcji</td><td class="center">2018-11-20</td><td >Nadolska Monika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Nadolska Monika</td></tr><tr class="line1 detail-grades" style="background-color: #F0E68C;"><td class="center">5+</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/11191915','ko',480,450)" >K</a></strong></td><td>Gimplus</td><td class="center">2018-11-25</td><td >Nadolska Monika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Nadolska Monika</td></tr><tr class="line1 detail-grades" style="background-color: #DC143C;"><td class="center">6-</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/11480723','ko',480,450)" >K</a></strong></td><td>Praca klasowa</td><td class="center">2018-11-27</td><td >Nadolska Monika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Nadolska Monika</td></tr><tr class="line1 detail-grades" style="background-color: #ADFF2F;"><td class="center">6</td><td class="center">&nbsp;</td><td>aktywność</td><td class="center">2018-12-04</td><td >Nadolska Monika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Nadolska Monika</td></tr><tr class="line1 detail-grades" style="background-color: #FF8C00;"><td class="center">2+</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/14177968','ko',480,450)" >K</a></strong></td><td>spr (oc. poprawiona)</td><td class="center">2018-12-11</td><td >Nadolska Monika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Nadolska Monika</td></tr><tr class="line1 detail-grades" style="background-color: #FFA07A;"><td class="center">5+</td><td class="center">&nbsp;</td><td>Sprawdzian</td><td class="center">2019-01-15</td><td >Nadolska Monika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >2+ (spr (oc. poprawiona))</td><td >Nadolska Monika</td></tr><tr class="line1 detail-grades" style="background-color: #DC143C;"><td class="center">6</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/17455093','ko',480,450)" >K</a></strong></td><td>Praca klasowa</td><td class="center">2019-01-02</td><td >Nadolska Monika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Nadolska Monika</td></tr><tr class="line1 detail-grades" style="background-color: #FFA07A;"><td class="center">5+</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/20665303','ko',480,450)" >K</a></strong></td><td>Sprawdzian</td><td class="center">2019-01-15</td><td >Nadolska Monika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Nadolska Monika</td></tr><tr class="line1 detail-grades" style="background-color: #DC143C;"><td class="center">6</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/21424519','ko',480,450)" >K</a></strong></td><td>Praca klasowa</td><td class="center">2019-01-18</td><td >Nadolska Monika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Nadolska Monika</td></tr><tr class="line1 detail-grades" style="background-color: #B0C4DE;"><td class="center">5-</td><td class="center">&nbsp;</td><td>śródroczna</td><td class="center">2019-01-21</td><td >Nadolska Monika</td><td class="center"><img src="/images/nieaktywne.png"  border="0" /></td><td class="right" ></td><td class="center" >-</td><td >Nadolska Monika</td></tr><tr class="line1 detail-grades" style="background-color: #ADFF2F;"><td class="center">6</td><td class="center">&nbsp;</td><td>aktywność</td><td class="center">2019-01-25</td><td >Nadolska Monika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Nadolska Monika</td></tr><tr class="line1 bolded"><td colspan="9" class="center">Okres 2</td></tr><tr class="line1 detail-grades" style="background-color: #FFA07A;"><td class="center">6</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/25253908','ko',480,450)" >K</a></strong></td><td>Sprawdzian</td><td class="center">2019-02-25</td><td >Nadolska Monika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Nadolska Monika</td></tr><tr class="line1 detail-grades" style="background-color: #ADFF2F;"><td class="center">6</td><td class="center">&nbsp;</td><td>aktywność</td><td class="center">2019-03-01</td><td >Nadolska Monika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Nadolska Monika</td></tr><tr class="line1 detail-grades" style="background-color: #32CD32;"><td class="center">6</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/26318073','ko',480,450)" >K</a></strong></td><td>kartkówka</td><td class="center">2019-03-04</td><td >Nadolska Monika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Nadolska Monika</td></tr><tr class="line1 detail-grades" style="background-color: #32CD32;"><td class="center">6</td><td class="center">&nbsp;</td><td>kartkówka</td><td class="center">2019-03-12</td><td >Nadolska Monika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Nadolska Monika</td></tr><tr class="line1 detail-grades" style="background-color: #ADFF2F;"><td class="center">+</td><td class="center">&nbsp;</td><td>aktywność</td><td class="center">2019-03-21</td><td >Nadolska Monika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Nadolska Monika</td></tr><tr class="line1 detail-grades" style="background-color: #ADFF2F;"><td class="center">6</td><td class="center">&nbsp;</td><td>aktywność</td><td class="center">2019-03-22</td><td >Nadolska Monika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Nadolska Monika</td></tr><tr class="line1 detail-grades" style="background-color: #ADFF2F;"><td class="center">+</td><td class="center">&nbsp;</td><td>aktywność</td><td class="center">2019-04-01</td><td >Nadolska Monika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Nadolska Monika</td></tr><tr class="line1 detail-grades" style="background-color: #FFA07A;"><td class="center">6</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/31619566','ko',480,450)" >K</a></strong></td><td>Sprawdzian</td><td class="center">2019-04-03</td><td >Nadolska Monika</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Nadolska Monika</td></tr></tbody><tfoot><tr><td  class="center" colspan="14"></td></tr></tfoot></table></td></tr><tr class="line1">
                            <td class='center micro screen-only'><img src="/images/tree_colapsed.png" id="przedmioty_32788_node" onclick="showHide.ShowHide(32788);" /></td>
                            <td >Religia</td><td ><span>[<span id="Ocena168" class="grade-box" style="background-color:#FFEBCD; ">
            <a title="Kategoria: inna<br>Data: 2018-09-27 (czw.)<br>Nauczyciel: Manderla fsc Dyr. Gimnazjum Piotr<br>Dodał: Manderla fsc Dyr. Gimnazjum Piotr<br/><br>Komentarz: Zeszyt IX" class="ocena" href="/przegladaj_oceny/szczegoly/1941651" >0</a></span><span id="Ocena169" class="grade-box" style="background-color:#FFEBCD; ">
            <a title="Kategoria: inna<br>Data: 2018-10-01 (pon.)<br>Nauczyciel: Manderla fsc Dyr. Gimnazjum Piotr<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Manderla fsc Dyr. Gimnazjum Piotr<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/2301451" >5+</a></span>]</span><span id="Ocena170" class="grade-box" style="background-color:#ADFF2F; ">
            <a title="Kategoria: aktywność<br>Data: 2018-10-25 (czw.)<br>Nauczyciel: Manderla fsc Dyr. Gimnazjum Piotr<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Manderla fsc Dyr. Gimnazjum Piotr<br/><br>Komentarz: Główne prawdy wiary" class="ocena" href="/przegladaj_oceny/szczegoly/6540203" >6</a></span><span id="Ocena171" class="grade-box" style="background-color:#FFEBCD; ">
            <a title="Kategoria: inna<br>Data: 2018-10-29 (pon.)<br>Nauczyciel: Manderla fsc Dyr. Gimnazjum Piotr<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Manderla fsc Dyr. Gimnazjum Piotr<br/><br>Komentarz: Zeszyt X" class="ocena" href="/przegladaj_oceny/szczegoly/6982074" >5+</a></span><span id="Ocena172" class="grade-box" style="background-color:#FFEBCD; ">
            <a title="Kategoria: inna<br>Data: 2018-11-29 (czw.)<br>Nauczyciel: Manderla fsc Dyr. Gimnazjum Piotr<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Manderla fsc Dyr. Gimnazjum Piotr<br/><br>Komentarz: Zeszyt XI" class="ocena" href="/przegladaj_oceny/szczegoly/11955710" >6</a></span><span id="Ocena173" class="grade-box" style="background-color:#FFEBCD; ">
            <a title="Kategoria: inna<br>Data: 2018-12-20 (czw.)<br>Nauczyciel: Manderla fsc Dyr. Gimnazjum Piotr<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Manderla fsc Dyr. Gimnazjum Piotr<br/><br>Komentarz: Zeszyt XII" class="ocena" href="/przegladaj_oceny/szczegoly/16688934" >5+</a></span><span id="Ocena174" class="grade-box" style="background-color:#FFEBCD; ">
            <a title="Kategoria: inna<br>Data: 2019-01-24 (czw.)<br>Nauczyciel: Manderla fsc Dyr. Gimnazjum Piotr<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Manderla fsc Dyr. Gimnazjum Piotr<br/><br>Komentarz: Zeszyt I" class="ocena" href="/przegladaj_oceny/szczegoly/22090141" >5+</a></span></td><td class="right">5.67</td><td class="center" > - </td><td class="center" ><span id="Ocena175" class="grade-box" style="background-color:#B0C4DE; ">
            <a title="Kategoria: śródroczna<br>Data: 2019-01-24 (czw.)<br>Nauczyciel: Manderla fsc Dyr. Gimnazjum Piotr<br>Licz do średniej: nie<br>Dodał: Manderla fsc Dyr. Gimnazjum Piotr<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/22091469" >6</a></span></td><td ><span id="Ocena176" class="grade-box" style="background-color:#FFEBCD; ">
            <a title="Kategoria: inna<br>Data: 2019-03-28 (czw.)<br>Nauczyciel: Manderla fsc Dyr. Gimnazjum Piotr<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Manderla fsc Dyr. Gimnazjum Piotr<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/30638165" >5</a></span></td><td  class="right">5.00</td><td class="center"  > - </td><td  class="right"  >5.57</td><td class="center"  > - </td><td class="center"  > - </td></tr><tr class="line1" name="przedmioty_all" id="przedmioty_32788" style="display: none; visibility: hidden;">
                              <td colspan="14" ><table class="stretch"><thead><tr class=""><td >Ocena</td><td title="Komentarz do oceny">K</td><td>Kategoria</td><td class="center">Data</td><td>Nauczyciel</td><td>Licz do śr.</td><td>Waga</td><td >Poprawa oceny</td><td>Dodał</td></tr></thead><tbody><tr class="line1 bolded"><td colspan="9" class="center">Okres 1</td></tr><tr class="line1 detail-grades" style="background-color: #FFEBCD;"><td class="center">0</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/1941651','ko',480,450)" >K</a></strong></td><td>inna</td><td class="center">2018-09-27</td><td >Manderla fsc Dyr. Gimnazjum Piotr</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Manderla fsc Dyr. Gimnazjum Piotr</td></tr><tr class="line1 detail-grades" style="background-color: #FFEBCD;"><td class="center">5+</td><td class="center">&nbsp;</td><td>inna</td><td class="center">2018-10-01</td><td >Manderla fsc Dyr. Gimnazjum Piotr</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >0 (inna)</td><td >Manderla fsc Dyr. Gimnazjum Piotr</td></tr><tr class="line1 detail-grades" style="background-color: #ADFF2F;"><td class="center">6</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/6540203','ko',480,450)" >K</a></strong></td><td>aktywność</td><td class="center">2018-10-25</td><td >Manderla fsc Dyr. Gimnazjum Piotr</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Manderla fsc Dyr. Gimnazjum Piotr</td></tr><tr class="line1 detail-grades" style="background-color: #FFEBCD;"><td class="center">5+</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/6982074','ko',480,450)" >K</a></strong></td><td>inna</td><td class="center">2018-10-29</td><td >Manderla fsc Dyr. Gimnazjum Piotr</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Manderla fsc Dyr. Gimnazjum Piotr</td></tr><tr class="line1 detail-grades" style="background-color: #FFEBCD;"><td class="center">6</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/11955710','ko',480,450)" >K</a></strong></td><td>inna</td><td class="center">2018-11-29</td><td >Manderla fsc Dyr. Gimnazjum Piotr</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Manderla fsc Dyr. Gimnazjum Piotr</td></tr><tr class="line1 detail-grades" style="background-color: #FFEBCD;"><td class="center">5+</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/16688934','ko',480,450)" >K</a></strong></td><td>inna</td><td class="center">2018-12-20</td><td >Manderla fsc Dyr. Gimnazjum Piotr</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Manderla fsc Dyr. Gimnazjum Piotr</td></tr><tr class="line1 detail-grades" style="background-color: #B0C4DE;"><td class="center">6</td><td class="center">&nbsp;</td><td>śródroczna</td><td class="center">2019-01-24</td><td >Manderla fsc Dyr. Gimnazjum Piotr</td><td class="center"><img src="/images/nieaktywne.png"  border="0" /></td><td class="right" ></td><td class="center" >-</td><td >Manderla fsc Dyr. Gimnazjum Piotr</td></tr><tr class="line1 detail-grades" style="background-color: #FFEBCD;"><td class="center">5+</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/22090141','ko',480,450)" >K</a></strong></td><td>inna</td><td class="center">2019-01-24</td><td >Manderla fsc Dyr. Gimnazjum Piotr</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Manderla fsc Dyr. Gimnazjum Piotr</td></tr><tr class="line1 bolded"><td colspan="9" class="center">Okres 2</td></tr><tr class="line1 detail-grades" style="background-color: #FFEBCD;"><td class="center">5</td><td class="center">&nbsp;</td><td>inna</td><td class="center">2019-03-28</td><td >Manderla fsc Dyr. Gimnazjum Piotr</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Manderla fsc Dyr. Gimnazjum Piotr</td></tr></tbody><tfoot><tr><td  class="center" colspan="14"></td></tr></tfoot></table></td></tr><tr class="line0">
                            <td class='center micro screen-only'><img src="/images/tree_colapsed.png" id="przedmioty_32790_node" onclick="showHide.ShowHide(32790);" /></td>
                            <td >Wiedza o społeczeństwie</td><td ><span id="Ocena177" class="grade-box" style="background-color:#87CEFA; ">
            <a title="Kategoria: samorząd terytorialny<br>Data: 2018-11-02 (pt.)<br>Nauczyciel: Zakarczemna Anna<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Zakarczemna Anna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/7675207" >3+</a></span><span id="Ocena178" class="grade-box" style="background-color:#66CDAA; ">
            <a title="Kategoria: refleksja obóz koncentracyjny<br>Data: 2018-11-19 (pon.)<br>Nauczyciel: Zakarczemna Anna<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Zakarczemna Anna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/9996766" >6</a></span><span id="Ocena179" class="grade-box" style="background-color:#FFA07A; ">
            <a title="Kategoria: prasówka<br>Data: 2018-11-20 (wt.)<br>Nauczyciel: Zakarczemna Anna<br>Dodał: Zakarczemna Anna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/10352165" >+</a></span><span id="Ocena180" class="grade-box" style="background-color:#FFA07A; ">
            <a title="Kategoria: prasówka<br>Data: 2018-11-27 (wt.)<br>Nauczyciel: Zakarczemna Anna<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Zakarczemna Anna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/11544985" >5+</a></span></td><td class="right">4.63</td><td class="center" > - </td><td class="center" ><span id="Ocena181" class="grade-box" style="background-color:#B0C4DE; ">
            <a title="Kategoria: śródroczna<br>Data: 2019-01-25 (pt.)<br>Nauczyciel: Zakarczemna Anna<br>Licz do średniej: nie<br>Dodał: Zakarczemna Anna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/22176520" >4+</a></span></td><td ><span id="Ocena182" class="grade-box" style="background-color:#7CFC00; ">
            <a title="Kategoria: historia najnowsza<br>Data: 2019-03-05 (wt.)<br>Nauczyciel: Zakarczemna Anna<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Zakarczemna Anna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/26584152" >5</a></span><span id="Ocena183" class="grade-box" style="background-color:#7CFC00; ">
            <a title="Kategoria: Polska i świat współczesny<br>Data: 2019-03-29 (pt.)<br>Nauczyciel: Zakarczemna Anna<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Zakarczemna Anna<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/30831639" >4</a></span></td><td  class="right">4.33</td><td class="center"  > - </td><td  class="right"  >4.50</td><td class="center"  > - </td><td class="center"  > - </td></tr><tr class="line0" name="przedmioty_all" id="przedmioty_32790" style="display: none; visibility: hidden;">
                              <td colspan="14" ><table class="stretch"><thead><tr class=""><td >Ocena</td><td title="Komentarz do oceny">K</td><td>Kategoria</td><td class="center">Data</td><td>Nauczyciel</td><td>Licz do śr.</td><td>Waga</td><td >Poprawa oceny</td><td>Dodał</td></tr></thead><tbody><tr class="line1 bolded"><td colspan="9" class="center">Okres 1</td></tr><tr class="line1 detail-grades" style="background-color: #87CEFA;"><td class="center">3+</td><td class="center">&nbsp;</td><td>samorząd terytorialny</td><td class="center">2018-11-02</td><td >Zakarczemna Anna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Zakarczemna Anna</td></tr><tr class="line1 detail-grades" style="background-color: #66CDAA;"><td class="center">6</td><td class="center">&nbsp;</td><td>refleksja obóz koncentracyjny</td><td class="center">2018-11-19</td><td >Zakarczemna Anna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Zakarczemna Anna</td></tr><tr class="line1 detail-grades" style="background-color: #FFA07A;"><td class="center">+</td><td class="center">&nbsp;</td><td>prasówka</td><td class="center">2018-11-20</td><td >Zakarczemna Anna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Zakarczemna Anna</td></tr><tr class="line1 detail-grades" style="background-color: #FFA07A;"><td class="center">5+</td><td class="center">&nbsp;</td><td>prasówka</td><td class="center">2018-11-27</td><td >Zakarczemna Anna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Zakarczemna Anna</td></tr><tr class="line1 detail-grades" style="background-color: #B0C4DE;"><td class="center">4+</td><td class="center">&nbsp;</td><td>śródroczna</td><td class="center">2019-01-25</td><td >Zakarczemna Anna</td><td class="center"><img src="/images/nieaktywne.png"  border="0" /></td><td class="right" ></td><td class="center" >-</td><td >Zakarczemna Anna</td></tr><tr class="line1 bolded"><td colspan="9" class="center">Okres 2</td></tr><tr class="line1 detail-grades" style="background-color: #7CFC00;"><td class="center">5</td><td class="center">&nbsp;</td><td>historia najnowsza</td><td class="center">2019-03-05</td><td >Zakarczemna Anna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Zakarczemna Anna</td></tr><tr class="line1 detail-grades" style="background-color: #7CFC00;"><td class="center">4</td><td class="center">&nbsp;</td><td>Polska i świat współczesny</td><td class="center">2019-03-29</td><td >Zakarczemna Anna</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Zakarczemna Anna</td></tr></tbody><tfoot><tr><td  class="center" colspan="14"></td></tr></tfoot></table></td></tr><tr class="line1">
                            <td class='center micro screen-only'><img src="/images/tree_colapsed.png" id="przedmioty_32791_node" onclick="showHide.ShowHide(32791);" /></td>
                            <td >Wychowanie do życia w rodzinie</td><td >Brak ocen</td><td class="right">-</td><td class="center" > - </td><td class="center" > - </td><td >Brak ocen</td><td  class="right">-</td><td class="center"  > - </td><td  class="right"  >-</td><td class="center"  > - </td><td class="center"  > - </td></tr><tr class="line1" name="przedmioty_all" id="przedmioty_32791" style="display: none; visibility: hidden;">
                              <td colspan="14" ><table class="stretch"><thead><tr class=""><td >Ocena</td><td title="Komentarz do oceny">K</td><td>Kategoria</td><td class="center">Data</td><td>Nauczyciel</td><td>Licz do śr.</td><td>Waga</td><td >Poprawa oceny</td><td>Dodał</td></tr></thead><tbody><tr class="line1"><td colspan="9" class="WierszTab2">Brak ocen</td></tr></tbody><tfoot><tr><td  class="center" colspan="14"></td></tr></tfoot></table></td></tr><tr class="line0">
                            <td class='center micro screen-only'><img src="/images/tree_colapsed.png" id="przedmioty_32792_node" onclick="showHide.ShowHide(32792);" /></td>
                            <td >Wychowanie fizyczne</td><td ><span id="Ocena184" class="grade-box" style="background-color:#ADFF2F; ">
            <a title="Kategoria: aktywność<br>Data: 2018-10-24 (śr.)<br>Nauczyciel: Brenk Tomasz<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Brenk Tomasz<br/><br>Komentarz: wrzesien" class="ocena" href="/przegladaj_oceny/szczegoly/6288781" >5</a></span><span id="Ocena185" class="grade-box" style="background-color:#ADFF2F; ">
            <a title="Kategoria: aktywność<br>Data: 2018-10-29 (pon.)<br>Nauczyciel: Brenk Tomasz<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Brenk Tomasz<br/><br>Komentarz: pazdziernik" class="ocena" href="/przegladaj_oceny/szczegoly/7066244" >5</a></span><span id="Ocena186" class="grade-box" style="background-color:#ADFF2F; ">
            <a title="Kategoria: aktywność<br>Data: 2018-11-05 (pon.)<br>Nauczyciel: Brenk Tomasz<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Brenk Tomasz<br/><br>Komentarz: turniej unihokej" class="ocena" href="/przegladaj_oceny/szczegoly/7892035" >5</a></span><span id="Ocena187" class="grade-box" style="background-color:#ADFF2F; ">
            <a title="Kategoria: aktywność<br>Data: 2018-11-05 (pon.)<br>Nauczyciel: Brenk Tomasz<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Brenk Tomasz<br/><br>Komentarz: pazdziernik" class="ocena" href="/przegladaj_oceny/szczegoly/7889126" >5</a></span><span id="Ocena188" class="grade-box" style="background-color:#F0E68C; ">
            <a title="Kategoria: Test Coopera<br>Data: 2018-11-05 (pon.)<br>Nauczyciel: Brenk Tomasz<br>Dodał: Brenk Tomasz<br/><br>Komentarz: 25 - 1750" class="ocena" href="/przegladaj_oceny/szczegoly/7877759" >+</a></span><span id="Ocena189" class="grade-box" style="background-color:#FF0000; ">
            <a title="Kategoria: sprawdzian<br>Data: 2019-01-16 (śr.)<br>Nauczyciel: Brenk Tomasz<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Brenk Tomasz<br/>Obowiązek wyk. zadania: TAK<br/><br>Komentarz: skok kuczny  przez skrzynie" class="ocena" href="/przegladaj_oceny/szczegoly/20944098" >5</a></span></td><td class="right">5.00</td><td class="center" > - </td><td class="center" ><span id="Ocena190" class="grade-box" style="background-color:#B0C4DE; ">
            <a title="Kategoria: śródroczna<br>Data: 2019-01-21 (pon.)<br>Nauczyciel: Brenk Tomasz<br>Licz do średniej: nie<br>Dodał: Brenk Tomasz<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/21668213" >5</a></span></td><td ><span id="Ocena191" class="grade-box" style="background-color:#32CD32; ">
            <a title="Kategoria: Frekwencja<br>Data: 2019-03-27 (śr.)<br>Nauczyciel: Brenk Tomasz<br>Licz do średniej: nie<br>Dodał: Brenk Tomasz<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/30289338" >5</a></span><span id="Ocena192" class="grade-box" style="background-color:#ADFF2F; ">
            <a title="Kategoria: aktywność<br>Data: 2019-03-27 (śr.)<br>Nauczyciel: Brenk Tomasz<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Brenk Tomasz<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/30288654" >5</a></span></td><td  class="right">5.00</td><td class="center"  > - </td><td  class="right"  >5.00</td><td class="center"  > - </td><td class="center"  > - </td></tr><tr class="line0" name="przedmioty_all" id="przedmioty_32792" style="display: none; visibility: hidden;">
                              <td colspan="14" ><table class="stretch"><thead><tr class=""><td >Ocena</td><td title="Komentarz do oceny">K</td><td>Kategoria</td><td class="center">Data</td><td>Nauczyciel</td><td>Licz do śr.</td><td>Waga</td><td >Poprawa oceny</td><td>Dodał</td></tr></thead><tbody><tr class="line1 bolded"><td colspan="9" class="center">Okres 1</td></tr><tr class="line1 detail-grades" style="background-color: #ADFF2F;"><td class="center">5</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/6288781','ko',480,450)" >K</a></strong></td><td>aktywność</td><td class="center">2018-10-24</td><td >Brenk Tomasz</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Brenk Tomasz</td></tr><tr class="line1 detail-grades" style="background-color: #ADFF2F;"><td class="center">5</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/7066244','ko',480,450)" >K</a></strong></td><td>aktywność</td><td class="center">2018-10-29</td><td >Brenk Tomasz</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Brenk Tomasz</td></tr><tr class="line1 detail-grades" style="background-color: #ADFF2F;"><td class="center">5</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/7892035','ko',480,450)" >K</a></strong></td><td>aktywność</td><td class="center">2018-11-05</td><td >Brenk Tomasz</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Brenk Tomasz</td></tr><tr class="line1 detail-grades" style="background-color: #ADFF2F;"><td class="center">5</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/7889126','ko',480,450)" >K</a></strong></td><td>aktywność</td><td class="center">2018-11-05</td><td >Brenk Tomasz</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Brenk Tomasz</td></tr><tr class="line1 detail-grades" style="background-color: #F0E68C;"><td class="center">+</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/7877759','ko',480,450)" >K</a></strong></td><td>Test Coopera</td><td class="center">2018-11-05</td><td >Brenk Tomasz</td><td class="center"><img src="/images/nieaktywne.png"  border="0" /></td><td class="right" ></td><td class="center" >-</td><td >Brenk Tomasz</td></tr><tr class="line1 detail-grades" style="background-color: #FF0000;"><td class="center">5</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/20944098','ko',480,450)" >K</a></strong></td><td>sprawdzian</td><td class="center">2019-01-16</td><td >Brenk Tomasz</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Brenk Tomasz</td></tr><tr class="line1 detail-grades" style="background-color: #B0C4DE;"><td class="center">5</td><td class="center">&nbsp;</td><td>śródroczna</td><td class="center">2019-01-21</td><td >Brenk Tomasz</td><td class="center"><img src="/images/nieaktywne.png"  border="0" /></td><td class="right" ></td><td class="center" >-</td><td >Brenk Tomasz</td></tr><tr class="line1 bolded"><td colspan="9" class="center">Okres 2</td></tr><tr class="line1 detail-grades" style="background-color: #32CD32;"><td class="center">5</td><td class="center">&nbsp;</td><td>Frekwencja</td><td class="center">2019-03-27</td><td >Brenk Tomasz</td><td class="center"><img src="/images/nieaktywne.png"  border="0" /></td><td class="right" ></td><td class="center" >-</td><td >Brenk Tomasz</td></tr><tr class="line1 detail-grades" style="background-color: #ADFF2F;"><td class="center">5</td><td class="center">&nbsp;</td><td>aktywność</td><td class="center">2019-03-27</td><td >Brenk Tomasz</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Brenk Tomasz</td></tr></tbody><tfoot><tr><td  class="center" colspan="14"></td></tr></tfoot></table></td></tr><tr class="line1">
                            <td class='center micro screen-only'><img src="/images/tree_colapsed.png" id="przedmioty_34012_node" onclick="showHide.ShowHide(34012);" /></td>
                            <td >Zajęcia artystyczne</td><td ><span id="Ocena193" class="grade-box" style="background-color:#A9A9A9; ">
            <a title="Kategoria: Śpiew<br>Data: 2018-10-22 (pon.)<br>Nauczyciel: Bradtke Agnieszka<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Bradtke Agnieszka<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/5933374" >5-</a></span><span>[<span id="Ocena194" class="grade-box" style="background-color:#FF1493; ">
            <a title="Kategoria: sprawdzian<br>Data: 2018-10-22 (pon.)<br>Nauczyciel: Bradtke Agnieszka<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Bradtke Agnieszka<br/>Obowiązek wyk. zadania: TAK<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/3398224" >3+</a></span><span id="Ocena195" class="grade-box" style="background-color:#FF1493; ">
            <a title="Kategoria: sprawdzian<br>Data: 2018-11-07 (śr.)<br>Nauczyciel: Bradtke Agnieszka<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Bradtke Agnieszka<br/>Obowiązek wyk. zadania: TAK<br/>&lt;br /&gt;Poprawa oceny: 3+ (sprawdzian)" class="ocena" href="/przegladaj_oceny/szczegoly/8440418" >5</a></span>]</span><span id="Ocena196" class="grade-box" style="background-color:#A9A9A9; ">
            <a title="Kategoria: Śpiew<br>Data: 2018-11-05 (pon.)<br>Nauczyciel: Bradtke Agnieszka<br>Licz do średniej: tak<br>Waga: 1<br>Dodał: Bradtke Agnieszka<br/><br>Komentarz: Rota" class="ocena" href="/przegladaj_oceny/szczegoly/7916787" >5</a></span><span id="Ocena197" class="grade-box" style="background-color:#FFF8DC; ">
            <a title="Kategoria: Praca na lekcji<br>Data: 2018-11-20 (wt.)<br>Nauczyciel: Bradtke Agnieszka<br>Licz do średniej: nie<br>Dodał: Bradtke Agnieszka<br/><br>Komentarz: wykreślanka" class="ocena" href="/przegladaj_oceny/szczegoly/10318627" >5</a></span><span id="Ocena198" class="grade-box" style="background-color:#FFF8DC; ">
            <a title="Kategoria: Praca na lekcji<br>Data: 2018-12-10 (pon.)<br>Nauczyciel: Bradtke Agnieszka<br>Licz do średniej: nie<br>Dodał: Bradtke Agnieszka<br/><br>Komentarz: dekoracje świąteczne" class="ocena" href="/przegladaj_oceny/szczegoly/13988321" >6</a></span><span id="Ocena199" class="grade-box" style="background-color:#BDB76B; ">
            <a title="Kategoria: Zadanie domowe<br>Data: 2019-01-14 (pon.)<br>Nauczyciel: Bradtke Agnieszka<br>Dodał: Bradtke Agnieszka<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/20499210" >+</a></span><span id="Ocena200" class="grade-box" style="background-color:#FFD700; ">
            <a title="Kategoria: Praca malarska<br>Data: 2019-01-14 (pon.)<br>Nauczyciel: Bradtke Agnieszka<br>Licz do średniej: tak<br>Waga: 2<br>Dodał: Bradtke Agnieszka<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/20497127" >5</a></span></td><td class="right">4.53</td><td class="center" > - </td><td class="center" ><span id="Ocena201" class="grade-box" style="background-color:#B0C4DE; ">
            <a title="Kategoria: śródroczna<br>Data: 2019-01-21 (pon.)<br>Nauczyciel: Bradtke Agnieszka<br>Licz do średniej: nie<br>Dodał: Bradtke Agnieszka<br/>" class="ocena" href="/przegladaj_oceny/szczegoly/21698125" >5</a></span></td><td ><span id="Ocena202" class="grade-box" style="background-color:#FF1493; ">
            <a title="Kategoria: sprawdzian<br>Data: 2019-02-01 (pt.)<br>Nauczyciel: Bradtke Agnieszka<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Bradtke Agnieszka<br/>Obowiązek wyk. zadania: TAK<br/><br>Komentarz: Romantyzm" class="ocena" href="/przegladaj_oceny/szczegoly/22216316" >6</a></span><span id="Ocena203" class="grade-box" style="background-color:#FF8C00; ">
            <a title="Kategoria: Ferie<br>Data: 2019-03-25 (pon.)<br>Nauczyciel: Bradtke Agnieszka<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Bradtke Agnieszka<br/>Obowiązek wyk. zadania: TAK<br/><br>Komentarz: Prezentacja multimedialna" class="ocena" href="/przegladaj_oceny/szczegoly/29873296" >6</a></span><span id="Ocena204" class="grade-box" style="background-color:#FF1493; ">
            <a title="Kategoria: sprawdzian<br>Data: 2019-04-01 (pon.)<br>Nauczyciel: Bradtke Agnieszka<br>Licz do średniej: tak<br>Waga: 3<br>Dodał: Bradtke Agnieszka<br/>Obowiązek wyk. zadania: TAK<br/><br>Komentarz: Realizm" class="ocena" href="/przegladaj_oceny/szczegoly/29930522" >5</a></span></td><td  class="right">5.67</td><td class="center"  > - </td><td  class="right"  >5.07</td><td class="center"  > - </td><td class="center"  > - </td></tr><tr class="line1" name="przedmioty_all" id="przedmioty_34012" style="display: none; visibility: hidden;">
                              <td colspan="14" ><table class="stretch"><thead><tr class=""><td >Ocena</td><td title="Komentarz do oceny">K</td><td>Kategoria</td><td class="center">Data</td><td>Nauczyciel</td><td>Licz do śr.</td><td>Waga</td><td >Poprawa oceny</td><td>Dodał</td></tr></thead><tbody><tr class="line1 bolded"><td colspan="9" class="center">Okres 1</td></tr><tr class="line1 detail-grades" style="background-color: #A9A9A9;"><td class="center">5-</td><td class="center">&nbsp;</td><td>Śpiew</td><td class="center">2018-10-22</td><td >Bradtke Agnieszka</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Bradtke Agnieszka</td></tr><tr class="line1 detail-grades" style="background-color: #FF1493;"><td class="center">3+</td><td class="center">&nbsp;</td><td>sprawdzian</td><td class="center">2018-10-22</td><td >Bradtke Agnieszka</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Bradtke Agnieszka</td></tr><tr class="line1 detail-grades" style="background-color: #FF1493;"><td class="center">5</td><td class="center">&nbsp;</td><td>sprawdzian</td><td class="center">2018-11-07</td><td >Bradtke Agnieszka</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >3+ (sprawdzian)</td><td >Bradtke Agnieszka</td></tr><tr class="line1 detail-grades" style="background-color: #A9A9A9;"><td class="center">5</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/7916787','ko',480,450)" >K</a></strong></td><td>Śpiew</td><td class="center">2018-11-05</td><td >Bradtke Agnieszka</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Bradtke Agnieszka</td></tr><tr class="line1 detail-grades" style="background-color: #FFF8DC;"><td class="center">5</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/10318627','ko',480,450)" >K</a></strong></td><td>Praca na lekcji</td><td class="center">2018-11-20</td><td >Bradtke Agnieszka</td><td class="center"><img src="/images/nieaktywne.png"  border="0" /></td><td class="right" ></td><td class="center" >-</td><td >Bradtke Agnieszka</td></tr><tr class="line1 detail-grades" style="background-color: #FFF8DC;"><td class="center">6</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/13988321','ko',480,450)" >K</a></strong></td><td>Praca na lekcji</td><td class="center">2018-12-10</td><td >Bradtke Agnieszka</td><td class="center"><img src="/images/nieaktywne.png"  border="0" /></td><td class="right" ></td><td class="center" >-</td><td >Bradtke Agnieszka</td></tr><tr class="line1 detail-grades" style="background-color: #BDB76B;"><td class="center">+</td><td class="center">&nbsp;</td><td>Zadanie domowe</td><td class="center">2019-01-14</td><td >Bradtke Agnieszka</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >1</td><td class="center" >-</td><td >Bradtke Agnieszka</td></tr><tr class="line1 detail-grades" style="background-color: #FFD700;"><td class="center">5</td><td class="center">&nbsp;</td><td>Praca malarska</td><td class="center">2019-01-14</td><td >Bradtke Agnieszka</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >2</td><td class="center" >-</td><td >Bradtke Agnieszka</td></tr><tr class="line1 detail-grades" style="background-color: #B0C4DE;"><td class="center">5</td><td class="center">&nbsp;</td><td>śródroczna</td><td class="center">2019-01-21</td><td >Bradtke Agnieszka</td><td class="center"><img src="/images/nieaktywne.png"  border="0" /></td><td class="right" ></td><td class="center" >-</td><td >Bradtke Agnieszka</td></tr><tr class="line1 bolded"><td colspan="9" class="center">Okres 2</td></tr><tr class="line1 detail-grades" style="background-color: #FF1493;"><td class="center">6</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/22216316','ko',480,450)" >K</a></strong></td><td>sprawdzian</td><td class="center">2019-02-01</td><td >Bradtke Agnieszka</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Bradtke Agnieszka</td></tr><tr class="line1 detail-grades" style="background-color: #FF8C00;"><td class="center">6</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/29873296','ko',480,450)" >K</a></strong></td><td>Ferie</td><td class="center">2019-03-25</td><td >Bradtke Agnieszka</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Bradtke Agnieszka</td></tr><tr class="line1 detail-grades" style="background-color: #FF1493;"><td class="center">5</td><td class="center"><strong><a href="javascript:void(0);"   onClick="otworz_w_nowym_oknie('/komentarz_oceny/1/29930522','ko',480,450)" >K</a></strong></td><td>sprawdzian</td><td class="center">2019-04-01</td><td >Bradtke Agnieszka</td><td class="center"><img src="/images/aktywne.png" border="0" /></td><td class="right" >3</td><td class="center" >-</td><td >Bradtke Agnieszka</td></tr></tbody><tfoot><tr><td  class="center" colspan="14"></td></tr></tfoot></table></td></tr><tr class="line0">
                            <td class='center micro screen-only'><img src="/images/tree_colapsed.png" id="przedmioty_75173_node" onclick="showHide.ShowHide(75173);" /></td>
                            <td >Zajęcia z wychowawcą</td><td >Brak ocen</td><td class="right">-</td><td class="center" > - </td><td class="center" > - </td><td >Brak ocen</td><td  class="right">-</td><td class="center"  > - </td><td  class="right"  >-</td><td class="center"  > - </td><td class="center"  > - </td></tr><tr class="line0" name="przedmioty_all" id="przedmioty_75173" style="display: none; visibility: hidden;">
                              <td colspan="14" ><table class="stretch"><thead><tr class=""><td >Ocena</td><td title="Komentarz do oceny">K</td><td>Kategoria</td><td class="center">Data</td><td>Nauczyciel</td><td>Licz do śr.</td><td>Waga</td><td >Poprawa oceny</td><td>Dodał</td></tr></thead><tbody><tr class="line1"><td colspan="9" class="WierszTab2">Brak ocen</td></tr></tbody><tfoot><tr><td  class="center" colspan="14"></td></tr></tfoot></table></td></tr>        <tr class="bolded line1">
            <td class="center micro screen-only">
                <img src="/images/tree_colapsed.png" id="przedmioty_zachowanie_node"
                                                     onclick="showHide.ShowHide('zachowanie');" />
            </td>
	        <td>
                Zachowanie
            </td>
            <td colspan="3">
                            &nbsp;
            </td>
            <td class="center">
                bardzo dobre            </td>
            <td colspan="2">
                            &nbsp;
            </td>
            <td class="center" colspan="4">
                -            </td>
        </tr>
                <tr class="line0" name="przedmioty_all" id="przedmioty_zachowanie"
                                                               style="display: none; visibility: hidden;">
            <td>&nbsp;</td>
            <td colspan="14" >
                <table class="stretch">
                    <thead>
                        <tr>
                            <td>Kategoria</td>
                            <td>Komentarz</td>
                            <td>Oceny</td>
                            <td>Data wystawienia</td>
                            <td>Dodał</td>
                                                    </tr>
                    </thead>
                    <tr class="line0 bolded"><td colspan="5" class="center">Okres 1</td></tr><tr class="line1"><td colspan="5" class="center">Brak ocen</td></tr>                    <tr class="line0">
                        <td  colspan="2" class="right">
                            Punkty startowe                        </td>
                        <td class="right">
                            0                        </td>
                        <td class="right" colspan="2">&nbsp;</td>
                    </tr>
                                        <tr class="line1" >
                        <td colspan="2" class="right">
                            <strong>
                                Suma                            </strong>
                        </td>
                        <td class="right">
                            <strong>
                                0                            </strong>
                        </td>
                        <td colspan="2">
                            &nbsp;                        </td>
                    <tr class="line0"><td colspan="2">&nbsp;</td>
                        <td>&nbsp;</td><td class="" colspan="3">Ocena śródroczna przewidywana wystawiona przez nauczyciela: </td><tr class="line1"><td colspan="2" class="right">Ocena opisowa śródroczna</td>
                    <td>&nbsp;</td><td class="" colspan="3"></td>
                </tr><tr class="line0 bolded"><td colspan="5"  class="center">Okres 2</td></tr><tr class="line1"><td colspan="5" class="center">Brak ocen</td></tr><tr class="line0"><td colspan="2" class="right">Punkty startowe</td>
                    <td class="right">0</td>
                    <td class="WierszTab2" colspan="2">&nbsp;</td>
                    </tr><tr class="line1"><td  colspan="2" class="right"><strong>Suma</strong></td>
                        <td class="right"><strong>0</strong></td><td colspan="2">&nbsp;</td>
                    </tr><tr class="line1">
                <td colspan="2">&nbsp;</td><td>&nbsp;</td>
                <td class="" colspan="3">Ocena roczna przewidywana wystawiona przez nauczyciela: </td></tr><tr class="line1"><td colspan="2" class="right">Ocena opisowa roczna</td>
                    <td>&nbsp;</td><td class="" colspan="3"></td>
                </tr></tbody><tfoot><tr ><td colspan="8"></td></tr></tfoot></table></td></tr></tbody><tfoot><tr><td colspan="12">&nbsp;</td></tr></tfoot></table><script type="text/javascript"> showHide.Show("zachowanie")</script>
        <div class='legend left stretch'>
    <h3>Legenda:</h3>
            <div style="display: inline-block; width: 100%;">
                <ul>
        <li class="double big" ><p><span>bz</span> - brak zadania</p></li><li class="double big" ><p><span>np</span> - nieprzygotowany</p></li><li class="double big" ><p><span>nk</span> - nieklasyfikowany</p></li><li class="double big" ><p><span>uł</span> - uczestniczył</p></li><li class="double big" ><p><span>nł</span> - nie uczestniczył</p></li><li class="double big" ><p><span>zl</span> - zaliczył</p></li><li class="double big" ><p><span>nz</span> - nie zaliczył</p></li><li class="double big" ><p><span>zw</span> - zwolniony</p></li><li class="double big" ><p><span>uc</span> - uczęszczał</p></li><li class="double big" ><p><span>nu</span> - nie uczęszczał</p></li><li class="double big" ><p><b>Śr.I</b> - Średnia ocen bieżących z pierwszego okresu</p></li><li class="double big" ><p><b>(I)</b> - Przewidywana ocena śródroczna z pierwszego okresu</p></li><li class="double big" ><p><b>I</b> - Ocena śródroczna z pierwszego okresu</p></li><li class="double big" ><p><b>Śr.II</b> - Średnia ocen bieżących z drugiego okresu</p></li><li class="double big" ><p><b>II</b> - Ocena śródroczna z drugiego okresu</p></li>
                <li class="double big" ><p><b>Śr.R</b> - Średnia ocen bieżących z całego roku szkolnego</p></li><li class="double big" ><p><b>(R)</b> - Przewidywana ocena roczna</p></li><li class="double big" ><p><b>R</b> - Ocena roczna</p></li><li {$listClass}><p><b>*</b> - Ocena poprawiona w innym semestrze</p></li>
                </ul>
            </div>
        
        </div>
    <p>&nbsp;</p></div></div></form><form id="go_back_form" method="POST" action="">    
    <input 
        type="hidden" 
        name="requestkey" 
        value="MC41MDE0MzgwMCAxNTU2NDU4NDY4XzdiYzcyNmE1YTdlZDQ3NmE0YTM4Y2EwYjk0N2Y5MmJk" 
    />

    <input type="hidden" name="Kid" value="" /><input type="hidden" name="UZid" value="" /><input type="hidden" name="Pid" value="" /><input type="hidden" name="oldKid" value="" /></form>        </div>
<script type="text/javascript">
    $(document).ready(function(){
        if( $('#body.have-interface').find(".container.static").length > 0 ) {
            $('#body').css("padding-left", $('#body').css("padding-right") );
        }
        $('#body.have-interface').removeClass("have-interface");
      });
  </script>
        <script type="text/javascript" src="/js/cookieBox.js"></script>
    <div id="cookieBox" class="cookieBox" style="display: none;">
        <div id="cookieBoxClose" class="cookieBoxClose"><a href="javascript: void(null);">x</a></div>
        <p>
            Ta strona wykorzystuje pliki cookies. Informacje zawarte w&nbsp;cookies wykorzystujemy m.in. w&nbsp;celach statystycznych, funkcjonalnych oraz dostosowania strony do&nbsp;indywidualnych potrzeb użytkownika. Dalsze korzystanie z&nbsp;serwisu oznacza, że&nbsp;zgadzasz&nbsp;się na&nbsp;ich&nbsp;zapisanie w&nbsp;pamięci Twojego urządzenia. Możesz samodzielnie zarządzać cookies zmieniając odpowiednio ustawienia w&nbsp;Twojej przeglądarce.<br/>
            Więcej informacji znajdziesz w <a class="cookies__link" href="https://synergia.librus.pl/polityka-prywatnosci/" target="_blank">Polityce prywatności</a>.
        </p>
    </div>
        <div id="footer">
              <hr></hr>
                <span id="bottom-logo"></span>
                </div>
  </div>
        </body>
    </html>
    
  
  '''



def interactive_legend(ax=None):
    if ax is None:
        ax = plt.gca()
    if ax.legend_ is None:
        ax.legend()

    return InteractiveLegend(ax.legend_)


class InteractiveLegend(object):
    def __init__(self, legend):
        self.legend = legend
        self.fig = legend.axes.figure

        self.lookup_artist, self.lookup_handle = self._build_lookups(legend)
        self._setup_connections()

        self.update()

    def _setup_connections(self):
        for artist in self.legend.texts + self.legend.legendHandles:
            artist.set_picker(10)  # 10 points tolerance

        self.fig.canvas.mpl_connect('pick_event', self.on_pick)
        self.fig.canvas.mpl_connect('button_press_event', self.on_click)

    def _build_lookups(self, legend):
        labels = [t.get_text() for t in legend.texts]
        handles = legend.legendHandles
        label2handle = dict(zip(labels, handles))
        handle2text = dict(zip(handles, legend.texts))

        lookup_artist = {}
        lookup_handle = {}
        for artist in legend.axes.get_children():
            if artist.get_label() in labels:
                handle = label2handle[artist.get_label()]
                lookup_handle[artist] = handle
                lookup_artist[handle] = artist
                lookup_artist[handle2text[handle]] = artist

        lookup_handle.update(zip(handles, handles))
        lookup_handle.update(zip(legend.texts, handles))

        return lookup_artist, lookup_handle

    def on_pick(self, event):
        handle = event.artist
        if handle in self.lookup_artist:
            artist = self.lookup_artist[handle]
            artist.set_visible(not artist.get_visible())
            self.update()

    def on_click(self, event):
        if event.button == 3:
            visible = False
        elif event.button == 2:
            visible = True
        else:
            return

        for artist in self.lookup_artist.values():
            artist.set_visible(visible)
        self.update()

    def update(self):
        for artist in self.lookup_artist.values():
            handle = self.lookup_handle[artist]
            if artist.get_visible():
                handle.set_visible(True)
            else:
                handle.set_visible(False)
        self.fig.canvas.draw()

    def show(self):
        plt.show()
#jaki syf ze stack overflowa==================================================
fig, ax = plt.subplots()

#plotowanie dancyh======================================================

#plotowanie dancyh======================================================




nazwy_przedmiotow = ['Biologia', 'Chemia', 'EBB', 'Fizyka', 'Geografia', 'Historia', 'Inf', 'ang', 'niem', 'pol',
                     'matematyka', 'Religia', 'Wos', 'Wf', 'zajarty']
soup = BeautifulSoup(html_string, 'lxml')  # Parse the HTML as a string

table = soup.find_all("a", class_="ocena")
i = 0
daty = []
oceny = []
przedmioty = []
wagi =[]
srodroczne = []
#wczytanie dancyh=============================================================

for row in table:
    i += 1
    zupa = str(row["title"]);
    if "Kategoria: przewidywana śródroczna" not in zupa and "Kategoria: śródroczna" not in zupa and 'Kategoria: przewidywana roczna' not in zupa and 'Ocena: brak zadania' not in zupa:
        data = (zupa.split("Data: ", 1)[-1]).split("(", 1)[0]
        nauczyciel = (zupa.split("Nauczyciel: ", 1)[-1]).split("<", 1)[0]
        waga = (zupa.split("Waga: ", 1)[-1]).split("<", 1)[0]
        ocena = str(row.text)
        if len(data) > 4:
            data = data.split("-")[1:3]
            if int(data[0]) > 6:
                data = ((int(data[0]) - 8) * 30) + int(data[1])
            else:
                data = ((int(data[0]) + 4)* 30) + int(data[1])


        if (ocena != "np" and ocena != "-" and ocena != "+" and ocena != "0"):
            daty.append(data)
            if len(ocena) == 1:
                oceny.append(int(ocena[0]))
            else:
                if ocena[1] == "+":
                    oceny.append(int(ocena[0]) + 0.5)
                if ocena[1] == "-":
                    oceny.append(int(ocena[0]) - 0.25)
            przedmioty.append(nauczyciel)
            if waga=='Kategoria: Praca na lekcji':
                waga='1'
            if waga == 'Kategoria: Frekwencja':
                waga ='d1'
            wagi.append(waga)
    if "Kategoria: śródroczna" in zupa:
        data = (zupa.split("Data: ", 1)[-1]).split("(", 1)[0]
        nauczyciel = (zupa.split("Nauczyciel: ", 1)[-1]).split("<", 1)[0]
        ocena = int(str(row.text)[0])
        if len(data) > 4:
            data = data.split("-")[1:3]
            if int(data[0]) > 6:
                data = ((int(data[0]) - 8) * 30) + int(data[1])
            else:
                data = ((int(data[0]) + 4)* 30) + int(data[1])
        srodroczne.append(ocena)
#polot ocen ==========================================================
#i=1
#for x in range(0, 15):
#    linia_ocena = []
#    linia_data = []
#    while True:
#        if len(przedmioty) - 2 < i:
#            break
#        if przedmioty[i + 1] == przedmioty[i]:
#            linia_ocena.append(oceny[i])
#            linia_data.append(daty[i])
#        else:
#            linia_ocena.append(oceny[i])
#            linia_data.append(daty[i])
#            break
#        i += 1
#    linia_data , linia_ocena = zip(*sorted(zip(linia_data,linia_ocena)))
#    ax.plot(linia_data, linia_ocena, label=nazwy_przedmiotow[x])
#    i += 1
#plt.xlabel('data')
#plt.ylabel('ocena')
#plt.title("Wykres ocen")
#plt.legend()
#interactive_legend().show()
#plotowanie sredniej
predicted = []
i = 1
ileocen = 0
avg_all = 0
for x in range(0, 15):
    linia_ocena = []
    linia_data = []
    while True:
        if len(przedmioty) - 2 < i:
            break
        if przedmioty[i + 1] == przedmioty[i]:
            linia_ocena.append(oceny[i])
            linia_data.append(daty[i])
            ileocen+=1
        else:
            linia_ocena.append(oceny[i])
            linia_data.append(daty[i])
            ileocen += 1
            break
        i += 1
    linia_data, linia_ocena = zip(*sorted(zip(linia_data, linia_ocena)))

    srednia = linia_ocena[0]
    elementy = 0
    waga_linia = wagi[ileocen-len(linia_ocena)+1:ileocen+1]
    linia_ocena = list(linia_ocena)
    nowa_linia_ocena =[]
    print(nazwy_przedmiotow[x] + " " + str(linia_ocena))
    for f in range(0,len(linia_ocena)):
        do_sredniej = linia_ocena[0:f+1]
        wagi_do_serdniej = waga_linia[0:f+1]
        print(do_sredniej)
        print(wagi_do_serdniej)
        dielenie = 0
        srednia = 0
        for k in range(0,len(do_sredniej)):
            for d in range(0,int(wagi_do_serdniej[k])):
                srednia+=do_sredniej[k]
                dielenie+=1
        nowa_linia_ocena.append(srednia/dielenie)

    print(nazwy_przedmiotow[x]+" "+str(linia_ocena))
    koncowa = 0
    if nowa_linia_ocena[len(nowa_linia_ocena)-1] - int(nowa_linia_ocena[len(nowa_linia_ocena)-1])>0.75:
        koncowa = int(nowa_linia_ocena[len(nowa_linia_ocena)-1])+1
    else:
        koncowa = int(nowa_linia_ocena[len(nowa_linia_ocena)-1])
    if nazwy_przedmiotow[x]=="Inf":
        koncowa+=1
    if nazwy_przedmiotow[x]=="ang":
        koncowa+=1
    if nazwy_przedmiotow[x]=="matematyka":
        koncowa+=1
    if nazwy_przedmiotow[x]=="zajarty":
        koncowa+=1
    if nazwy_przedmiotow[x]=="Religia":
        koncowa+=1
    predicted.append(koncowa)

    ax.plot(linia_data, linia_ocena, label=nazwy_przedmiotow[x]+" "+str(nowa_linia_ocena[len(nowa_linia_ocena)-1])[0:4]+" "+"końcowa: "+str(koncowa))
    avg_all +=nowa_linia_ocena[len(linia_ocena)-1]
    i += 1
plt.xlabel('data')
plt.ylabel('ocena')
print(predicted)
plt.title("ogolna średnia"+" "+str(avg_all/15)[0:4]+" "+"predicted"+" "+str(np.mean(predicted))+" "+"środroczne"+" "+str(np.mean(srodroczne)))

plt.legend()
ax.grid(color='grey', linestyle='-', linewidth=0.25, alpha=0.5)
interactive_legend().show()













